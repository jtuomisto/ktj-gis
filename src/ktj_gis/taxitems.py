from dataclasses import dataclass
from datetime import date


@dataclass
class TaxItem:
    pass


@dataclass
class Yhteenveto(TaxItem):
    """01.01.02"""
    tietotyyppi: str
    tiedosto_muodostettu: date
    verovuosi: int
    tietueiden_maara: int


@dataclass
class Kiinteisto(TaxItem):
    """12.06.02"""
    kiinteisto_pinta_ala: int
    tieto_pvm: date
    sijaintitunnus: str = None
    kuntatunnus: int = None
    sijaintikunta: str = None
    kiinteisto_nimi: str = None


@dataclass
class Omistaja(TaxItem):
    """12.07.03"""
    muu_hallintaoikeus: bool
    tieto_pvm: date
    sijaintitunnus: str = None
    maapohja_numero: int = None
    rakennus_numero: int = None
    muu_profiili_numero: int = None
    tunnus: str = None
    nimi: str = None
    omistusosuus: str = None
    hallintaosuus: str = None
    verotuksen_paattymis_pvm: date = None
    verotuksen_oikaisu_pvm: date = None


@dataclass
class Maapohja(TaxItem):
    """12.08.05"""
    maapohja_numero: int
    maapohja_tyyppi: str
    maapohja_pinta_ala: int
    rakennusoikeus: int
    tehokkuusluku: str
    alennuskaava: bool
    veroton: bool
    hinta_aluetunnus: str
    aluehinta: str
    verotusarvo: str
    kiinteistovero: str
    tieto_pvm: date
    sijaintitunnus: str = None
    maapohja_kayttotarkoitus: str = None
    kaava_laji: str = None
    kaava_kayttotarkoitus: str = None
    kaava_yksikkotunnus: str = None
    ranta: str = None
    laskentaperuste: int = None


@dataclass
class Rakennus(TaxItem):
    """12.09.04"""
    rakennus_numero: int
    kayttotarkoitus: str
    purkukuntoinen: bool
    kayttokelvoton: bool
    veroton: bool
    yleishyodyllinen: bool
    verotusarvo: str
    kiinteistovero: str
    tieto_pvm: date
    sijaintitunnus: str = None
    prt: str = None
    rakennus_tyyppi: int = None


@dataclass
class RakennusOsa(TaxItem):
    """12.10.03"""
    rakennus_numero: int
    rakennus_osa_numero: int
    rakennus_tyyppi: str
    pinta_ala: int
    tilavuus: int
    tieto_pvm: date
    sijaintitunnus: str = None
    kantava_rakenne: str = None
    rakennus_muoto: str = None
    rakentaminen_aloitettu_pvm: date = None
    rakentaminen_valmis_pvm: date = None
    rakentaminen_valmiusaste: int = None
    peruskorjausvuosi: int = None
    ika_alennus_laskentavuosi: int = None
    viimeistelematon_kellari_pinta_ala: int = None
    kuisti_pinta_ala: int = None
    varasto_pinta_ala: int = None
    hissikuilu_pinta_ala: int = None
    kellari_pinta_ala: int = None
    paikoitusalue_pinta_ala: int = None
    siilo_tyyppi: str = None
    siilo_halkaisija: str = None
    sahko: bool = None
    lammitys: str = None
    vesijohto: bool = None
    viemari: bool = None
    talviasuttava: bool = None
    vessa: bool = None
    sauna: bool = None
    hissi: bool = None
    ilmastointi: int = None
    lammitys_ja_vesi: int = None
    kasvihuone_rakenne_ja_lammitys: str = None
    kevytrakenteinen_ruokala: bool = None
    pysakointitalo_katettu_ylin_kerros: bool = None
    lampoeristys: bool = None


@dataclass
class MuuProfiili(TaxItem):
    """12.11.03"""
    muu_profiili_numero: int
    muu_profiili_tyyppi: str
    vesivoima_arvo: str
    verotusarvo: str
    kiinteistovero: str
    tieto_pvm: date
    sijaintitunnus: str = None
