import sys
import os
import argparse
import warnings
import traceback
import time
from datetime import datetime
from itertools import chain

if (pl := os.environ.get('PROJ_LIB')) is not None:
    print('PROJ_LIB is set, can possibly lead to proj related errors:')
    print(pl)

from osgeo import gdal, ogr, osr
from ktj_gis.subcommands import *
from .ktjitems import *


PROGRESS = {'count': 0}
TYPES = {
    'rekisteriyksikko': {Rekisteriyksikko, RekisteriyksikkoKiinteistoraja, RekisteriyksikkoRajamerkki},
    'rekisteriyksikko_kaava': {RekisteriyksikkoKaava},
    'palsta': {Rekisteriyksikko, Palsta},
    'muodostuminen': {Rekisteriyksikko, Muodostuminen, Muodostumistoimenpide},
    'määräala': {MaaraAla, MaaraAlaRekisteriyksikko, MaaraAlaOsa},
    'käyttöoikeusyksikkö': {Kayttooikeusyksikko, KayttooikeusyksikkoOsa},
    'kiinteistöraja': {Kiinteistoraja},
    'rajamerkki': {Rajamerkki},
    'yhteystiedot': {Henkilo, Yhteystieto, Yhteystiedot}
}


def parse_args(arguments: list) -> argparse.Namespace:
    pr = argparse.ArgumentParser(description='Tool for reading and syncing KTJ-data into a PostgreSQL database',
                                 allow_abbrev=False)

    pr.add_argument('--debug', action='store_true',
                    help='Print more verbose errors')
    pr.add_argument('--no-progress', action='store_true',
                    help='Disable messages about progress')

    spr = pr.add_subparsers(dest='command', required=True)

    # Database schema creation
    db_parser = spr.add_parser('database',
                               help='Initialize database (create tables, views)')
    db_parser.add_argument('-c', '--config', required=True,
                           help='Path to config file')
    db_parser.add_argument('--create-tables', action='store_true',
                           help='Create database tables')
    db_parser.add_argument('--create-views', action='store_true',
                           help='Create views')
    db_parser.add_argument('--import-definitions', action='store_true',
                           help='Import text definitions for numeric or enumerated text fields')
    db_parser.add_argument('--ktj', action='store_true',
                           help='Do specified actions for ktj')
    db_parser.add_argument('--takeover', action='store_true',
                           help='Do specified actions for parcel takeovers')
    db_parser.add_argument('--tax', action='store_true',
                           help='Do specified actions for taxes')

    # Options for reading and writing KTJ-xml files to Postgres
    read_xml_parser = spr.add_parser('read-xml',
                                     help='Read KTJ-xml files into database',
                                     epilog=f'Possible values for --types: {", ".join(TYPES.keys())}')
    read_xml_parser.add_argument('-c', '--config', required=True,
                                 help='Path to config file')
    read_xml_parser.add_argument('--types', nargs='+', choices=TYPES.keys(), metavar='TYPE',
                                 help='Read items of these types only (use at your own risk)')
    read_xml_parser.add_argument('paths', nargs='+',
                                 help='Paths to zip files, or directories of zipped files')

    # Concate directory into one xml-file
    concat_parser = spr.add_parser('concate',
                                   help='Concatenate a directory of KTJ-xml files into one')
    concat_parser.add_argument('-o', '--output', required=True,
                               help='Output file')
    concat_parser.add_argument('-m', '--millimeters', action='store_true',
                               help='Convert precision values to millimeters')
    concat_parser.add_argument('path',
                               help='Path to a directory of zipped ktj-data')

    # Download KTJ-xml data
    download_parser = spr.add_parser('download',
                                     help='Download KTJ-xml message files from the transfer service')
    download_parser.add_argument('-o', '--output', required=True,
                                 help='Output directory')
    download_parser.add_argument('-c', '--config', required=True,
                                 help='Path to config file')
    download_parser.add_argument('--after', metavar='DATE',
                                 help='Download changes after this date, iso format')
    download_parser.add_argument('--dry-run', action='store_true',
                                 help='List files that would be downloaded by this operation')
    download_group = download_parser.add_mutually_exclusive_group(required=True)
    download_group.add_argument('--full', action='store_true',
                                help='Download full release')
    download_group.add_argument('--changes', action='store_true',
                                help='Download changed release')
    download_type_group = download_parser.add_mutually_exclusive_group()
    download_type_group.add_argument('--cadastral', action='store_true',
                                     help='Download only cadastral data')
    download_type_group.add_argument('--contact', action='store_true',
                                     help='Download only contact data')

    # Update daemon
    daemon_parser = spr.add_parser('daemon',
                                   help='Run update daemon; database must be initialized first with the database subcommand')
    daemon_parser.add_argument('-d', '--data-dir', required=True,
                               help='Directory where to keep downloaded files and state info')
    daemon_parser.add_argument('-c', '--config', required=True,
                               help='Path to config file')
    daemon_parser.add_argument('--takeover', action='store_true',
                               help='Update parcel takeover table (database must be initialized with --takeover)')
    daemon_group = daemon_parser.add_mutually_exclusive_group(required=True)
    daemon_group.add_argument('--daily', action='store_true',
                              help='Update daily')
    daemon_group.add_argument('--weekly', action='store_true',
                              help='Update weekly')

    # Parcel takeovers
    extra_parser = spr.add_parser('takeover',
                                  help='Parcel takeover register (database must be initialized with --takeover)')
    extra_parser.add_argument('-c', '--config', required=True,
                              help='Path to config file')
    extra_parser.add_argument('--first-run', action='store_true',
                              help='This is a first run, do not add contacts')

    # Tax
    tax_parser = spr.add_parser('tax',
                                help='Read tax-data into database (database must be initialized with --tax)')
    tax_parser.add_argument('-c', '--config', required=True,
                            help='Path to config file')
    tax_parser.add_argument('-f', '--force', action='store_true',
                            help='Insert new data even if tables are populated')
    tax_parser.add_argument('path',
                            help='Path to a csv')

    return pr.parse_args(arguments)


def print_err(*args):
    message = args[0]

    if len(args) > 1:
        message = message.format(*args[1:])

    sys.stderr.write(message)
    sys.stderr.write(os.linesep)


def show_warning(message, category, filename, lineno, file=None, line=None):
    print('Warning: {} ({}:{})'.format(message, os.path.basename(filename), lineno))


def progress_creator(args: argparse.Namespace, divisor: int=5000, subject: str='items'):
    if args.no_progress:
        return None

    def _prg():
        PROGRESS['count'] += 1

        if PROGRESS['count'] % divisor == 0:
            print(f'Processed {PROGRESS['count']} {subject}...')

    return _prg


def main(*arguments):
    gdal.PushErrorHandler('CPLQuietErrorHandler')
    gdal.UseExceptions()
    ogr.UseExceptions()
    osr.UseExceptions()

    warnings.showwarning = show_warning

    if len(arguments) == 0:
        arguments = sys.argv[1:]

    try:
        args = parse_args(arguments)

        if 'after' in args and args.after is not None:
            args.after = datetime.fromisoformat(args.after)

        if 'types' in args and args.types is not None:
            args.types = set(chain.from_iterable(map(lambda x: TYPES[x], args.types)))
    except Exception as e:
        print_err('{}: Failed to parse arguments, {}', type(e).__name__, e)
        sys.exit(2)

    try:
        if args.command == 'daemon':
            print('Running KTJ update daemon...')
            cli_daemon(args)
            return

        start = time.perf_counter()

        match args.command:
            case 'read-xml':
                print('Reading KTJ-data into postgres...')
                cli_read_xml(args, progress=progress_creator(args, 5000))
            case 'database':
                print('Setting up the database...')
                cli_database(args)
            case 'concate':
                print('Concatenating xml-files...')
                cli_concatenate(args, progress=progress_creator(args, 5, subject='files'))
            case 'download':
                print('Downloading xml-files...')
                cli_download(args, progress=progress_creator(args, 5, subject='files'))
            case 'takeover':
                print('Updating parcel takeover table...')
                cli_takeover(args, progress=progress_creator(args, 20, subject='parcels'))
            case 'tax':
                print('Reading tax-data into postgres...')
                cli_tax(args, progress=progress_creator(args, 4000, subject='lines'))

        end = time.perf_counter()

        print(f'Finished in {end - start:.3f} seconds')
    except Exception as e:
        if args.debug and e.__cause__ is not None:
            print_err('{}: {}', type(e.__cause__).__name__, e.__cause__)
            traceback.print_tb(e.__cause__.__traceback__)

        print_err('{}: {}', type(e).__name__, e)

        if args.debug:
            traceback.print_tb(e.__traceback__)

        sys.exit(1)
