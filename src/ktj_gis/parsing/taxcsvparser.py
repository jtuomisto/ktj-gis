import csv
from collections.abc import Iterator
from datetime import date
from ktj_gis.taxitems import TaxItem, Yhteenveto
from .taxitemparser import *


class TaxCSVParser:
    encoding = 'ansi'
    dialect = {'delimiter': ';', 'quoting': csv.QUOTE_NONE}

    def __init__(self, file: str):
        self._file = file
        self._creation_date = None

    def _parse_item(self, item: list) -> TaxItem:
        full_item_type, *data = item

        try:
            item_type, _, version = full_item_type.rpartition('.')
            version = int(version)
        except (ValueError, TypeError, IndexError):
            raise ValueError(f'Unknown tax item type: {full_item_type}')

        match item_type:
            case '01.01.02':
                return YhteenvetoParser(version).parse(data)
            case '12.06.02':
                return KiinteistoParser(version).parse(data, self._creation_date)
            case '12.07.03':
                return OmistajaParser(version).parse(data, self._creation_date)
            case '12.08.05':
                return MaapohjaParser(version).parse(data, self._creation_date)
            case '12.09.04':
                return RakennusParser(version).parse(data, self._creation_date)
            case '12.10.03':
                return RakennusOsaParser(version).parse(data, self._creation_date)
            case '12.11.03':
                return MuuProfiiliParser(version).parse(data, self._creation_date)
            case _:
                raise ValueError(f'Unknown tax item type: {item_type}')

    def parse(self) -> Iterator[TaxItem]:
        with open(self._file, mode='r', newline='', encoding=self.encoding) as handle:
            reader = csv.reader(handle, **self.dialect)

            for item in reader:
                i = self._parse_item(item)

                if isinstance(i, Yhteenveto):
                    if i.tietotyyppi != 'REAL ESTATE':
                        raise ValueError(f'Wrong taxation type: {i.tietotyyppi}')

                    self._creation_date = i.tiedosto_muodostettu
                    continue

                yield i
