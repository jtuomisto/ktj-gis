import math
from typing import Iterator
from itertools import chain
from osgeo import ogr, osr
from lxml.etree import Element
from ktj_gis.misc import element_get_tag, element_get_text


def _coordinate_split(text: str, sep: str) -> Iterator[str]:
    return filter(None, map(lambda x: x.strip(), text.split(sep)))


def parse_gml_point(el: Element, srs: osr.SpatialReference) -> ogr.Geometry:
    coords = element_get_text(el, 'pos')

    if coords is None:
        raise ValueError('Missing point coordinates')

    x, y = _coordinate_split(coords, ' ')
    g = ogr.Geometry(ogr.wkbPoint)
    g.SetPoint_2D(0, float(x), float(y))

    if srs.EPSGTreatsAsNorthingEasting():
        g.SwapXY()

    g.AssignSpatialReference(srs)
    return g


def parse_gml_line(el: Element, srs: osr.SpatialReference, as_ring: bool=False) -> ogr.Geometry:
    coords = element_get_text(el, 'coordinates')

    if coords is None:
        raise ValueError('Missing line coordinates')

    geometry_type = ogr.wkbLinearRing if as_ring else ogr.wkbLineString
    g = ogr.Geometry(geometry_type)

    for vertex in _coordinate_split(coords, ' '):
        try:
            x, y = _coordinate_split(vertex, ',')
        except (AttributeError, ValueError):
            raise ValueError(f'Invalid coordinates for line: {coords}')

        g.AddPoint_2D(float(x), float(y))

    if srs.EPSGTreatsAsNorthingEasting():
        g.SwapXY()

    g.AssignSpatialReference(srs)

    if not as_ring:
        gc = ogr.Geometry(ogr.wkbCompoundCurve)
        gc.AddGeometry(g)
        gc.AssignSpatialReference(srs)
        return gc

    return g


def parse_gml_arc(el: Element, srs: osr.SpatialReference) -> ogr.Geometry:
    points = el.findall('{*}pos')

    if len(points) != 3:
        raise ValueError('Missing arc coordinates')

    arc_points = []

    for p in points:
        coords = element_get_text(p)

        if coords is None:
            raise ValueError('Missing arc coordinates')

        x, y = _coordinate_split(coords, ' ')
        ag = ogr.Geometry(ogr.wkbPoint)
        ag.SetPoint_2D(0, float(x), float(y))

        if srs.EPSGTreatsAsNorthingEasting():
            ag.SwapXY()

        arc_points.append(ag)

    start, center, end = arc_points
    g = ogr.Geometry(ogr.wkbCircularString)

    g.AddPoint_2D(start.GetX(), start.GetY())

    if start.Equals(end):
        g.AddPoint_2D(center.GetX(), center.GetY())
        g.AddPoint_2D(end.GetX(), end.GetY())
    else:
        radius = center.Distance(end)
        mid_x = (start.GetX() + end.GetX()) / 2
        mid_y = (start.GetY() + end.GetY()) / 2
        mid_angle = math.atan2(mid_y - center.GetY(), mid_x - center.GetX())
        x = center.GetX() + math.cos(mid_angle) * radius
        y = center.GetY() + math.sin(mid_angle) * radius
        g.AddPoint_2D(x, y)
        g.AddPoint_2D(end.GetX(), end.GetY())

    gc = ogr.Geometry(ogr.wkbCompoundCurve)
    gc.AddGeometry(g)
    gc.AssignSpatialReference(srs)

    return gc


def parse_gml_polygon(el: Element, srs: osr.SpatialReference) -> ogr.Geometry:
    g = ogr.Geometry(ogr.wkbPolygon)

    exterior = el.iterfind('{*}exterior')
    interior = el.iterfind('{*}interior')

    for e in chain.from_iterable(chain(exterior, interior)):
        tag = element_get_tag(e)

        if tag == 'LinearRing':
            g.AddGeometry(parse_gml_line(e, srs, as_ring=True))
        else:
            raise ValueError(f'Unhandled polygon ring: {tag}')

    g.AssignSpatialReference(srs)
    return g


def parse_ktj_geometry(el: Element, srs: osr.SpatialReference, only_of_type: int=None, linearize: bool=False) -> list[ogr.Geometry]:
    geometries = []
    location = el.find('{*}sijainti')

    if location is None:
        return geometries

    for e in location:
        tag = element_get_tag(e)

        match tag:
            case 'Piste':
                g = parse_gml_point(e, srs)
            case 'Murtoviiva' | 'Kayraviiva':
                g = parse_gml_line(e, srs)
            case 'Ympyrankaari':
                g = parse_gml_arc(e, srs)
            case 'Alue':
                g = parse_gml_polygon(e, srs)
            case _:
                continue

        if linearize:
            g = g.GetLinearGeometry()

        if only_of_type is None or g.GetGeometryType() == only_of_type:
            geometries.append(g)

    return geometries
