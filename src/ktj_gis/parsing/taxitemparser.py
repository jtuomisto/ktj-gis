from abc import abstractmethod, ABC
from warnings import warn
from datetime import date, datetime
from ktj_gis.taxitems import *
from .ktjcontactparser import id_hash


class TaxItemParser(ABC):
    versions = {None}

    def __init__(self, version: int):
        if version is None or version not in self.versions:
            raise ValueError(f'Unknown version for {self.__class__.__name__}: {version}')

        self._ver = version

    def _format_customer_id(self, cid: str) -> str:
        if cid is None:
            return None

        return id_hash(cid.strip())

    def _format_unit_id(self, uid: str) -> str:
        if uid is None:
            return None

        uid = uid.strip()

        if len(uid) == 0:
            return None

        parts = uid.split('-')
        formatted = '-'.join([str(int(x)) for x in parts[:4]])

        if len(parts) == 5:
            formatted += 'M' + str(int(parts[-1][1:]))

        return formatted

    def _format_fraction(self, n: str, d: str) -> str:
        if n is None or d is None:
            return None

        n, d = n.strip(), d.strip()

        if len(n) == 0 or len(d) == 0:
            return None

        return f'{int(n)}/{int(d)}'

    def _format_numeric(self, value: str) -> str:
        if value is None:
            return None

        value = value.strip()               # Data is in Finnish locale
        return None if len(value) == 0 else value.replace(',', '.', 1)

    def _parse_date(self, date: str) -> date:
        if date is None:
            return None

        date = date.strip()
        length = len(date)

        if length == 0:
            return None
        if length == 7:
            date = f'0{date}'
        elif length != 8:
            warn(f'Invalid date string: {date}')
            return None

        try:
            return datetime.strptime(date, '%d%m%Y').date()
        except (OSError, ValueError):
            warn(f'Invalid date string: {date}')
            return None

    def _parse_bool(self, value: str) -> bool:
        if value is None:
            return None

        match value.strip():
            case '0': return False
            case '1': return True
            case _: return None

    def _parse_int(self, value: str) -> int:
        if value is None:
            return None

        value = value.strip()
        return None if len(value) == 0 else int(value)

    def _parse_str(self, value: str) -> str:
        if value is None:
            return None

        value = value.strip()
        return None if len(value) == 0 else value

    @abstractmethod
    def parse(self, data: list[str], creation_date: date=None) -> TaxItem:
        raise NotImplementedError()


class YhteenvetoParser(TaxItemParser):
    versions = {17}

    def parse(self, data: list[str]) -> Yhteenveto:
        return Yhteenveto(
            tietotyyppi=self._parse_str(data[0]),
            tiedosto_muodostettu=self._parse_date(data[1]),
            verovuosi=self._parse_int(data[2]),
            tietueiden_maara=self._parse_int(data[3])
        )


class KiinteistoParser(TaxItemParser):
    versions = {19}

    def parse(self, data: list[str], creation_date: date) -> Kiinteisto:
        return Kiinteisto(
            tieto_pvm=creation_date,
            kiinteisto_pinta_ala=self._parse_int(data[4]),
            sijaintitunnus=self._format_unit_id(data[0]),
            kuntatunnus=self._parse_int(data[1]),
            sijaintikunta=self._parse_str(data[2]),
            kiinteisto_nimi=self._parse_str(data[3])
        )


class OmistajaParser(TaxItemParser):
    versions = {19, 21}

    def parse(self, data: list[str], creation_date: date) -> Omistaja:
        return Omistaja(
            tieto_pvm=creation_date,
            muu_hallintaoikeus=self._parse_bool(data[10]),
            sijaintitunnus=self._format_unit_id(data[0]),
            maapohja_numero=self._parse_int(data[1]),
            rakennus_numero=self._parse_int(data[2]),
            muu_profiili_numero=self._parse_int(data[3]),
            tunnus=self._format_customer_id(data[4]),
            nimi=self._parse_str(data[5]),
            omistusosuus=self._format_fraction(data[6], data[7]),
            hallintaosuus=self._format_fraction(data[8], data[9]),
            verotuksen_paattymis_pvm=self._parse_date(data[11]),
            verotuksen_oikaisu_pvm=self._parse_date(data[12])
        )


class MaapohjaParser(TaxItemParser):
    versions = {19, 21}

    def parse(self, data: list[str], creation_date: date) -> Maapohja:
        return Maapohja(
            tieto_pvm=creation_date,
            maapohja_numero=self._parse_int(data[1]),
            maapohja_tyyppi=self._parse_str(data[2]),
            maapohja_pinta_ala=self._parse_int(data[4]),
            rakennusoikeus=self._parse_int(data[5]),
            tehokkuusluku=self._format_numeric(data[6]),
            laskentaperuste=self._parse_int(data[11]),
            alennuskaava=self._parse_bool(data[14]),
            veroton=self._parse_bool(data[15]),
            hinta_aluetunnus=self._parse_str(data[12]),
            aluehinta=self._format_numeric(data[13]),
            verotusarvo=self._format_numeric(data[16]),
            kiinteistovero=self._format_numeric(data[17]),
            sijaintitunnus=self._format_unit_id(data[0]),
            maapohja_kayttotarkoitus=self._parse_str(data[3]),
            kaava_laji=self._parse_str(data[7]),
            kaava_kayttotarkoitus=self._parse_str(data[8]),
            kaava_yksikkotunnus=self._parse_str(data[10]),
            ranta=self._parse_str(data[9])
        )


class RakennusParser(TaxItemParser):
    versions = {19}

    def parse(self, data: list[str], creation_date: date) -> Rakennus:
        return Rakennus(
            tieto_pvm=creation_date,
            rakennus_numero=self._parse_int(data[1]),
            kayttotarkoitus=self._parse_str(data[4]),
            purkukuntoinen=self._parse_bool(data[5]),
            kayttokelvoton=self._parse_bool(data[6]),
            veroton=self._parse_bool(data[7]),
            yleishyodyllinen=self._parse_bool(data[8]),
            verotusarvo=self._format_numeric(data[9]),
            kiinteistovero=self._format_numeric(data[10]),
            sijaintitunnus=self._format_unit_id(data[0]),
            prt=self._parse_str(data[2]),
            rakennus_tyyppi=self._parse_int(data[3])
        )


class RakennusOsaParser(TaxItemParser):
    versions = {19, 21}

    def parse(self, data: list[str], creation_date: date) -> RakennusOsa:
        if self._ver < 21: # rakentaminen_valmiusaste does not exists before 21
            data.insert(6, '')

        return RakennusOsa(
            tieto_pvm=creation_date,
            rakennus_numero=self._parse_int(data[1]),
            rakennus_osa_numero=self._parse_int(data[2]),
            rakennus_tyyppi=self._parse_str(data[3]),
            pinta_ala=self._parse_int(data[7]),
            tilavuus=self._parse_int(data[8]),
            kantava_rakenne=self._parse_str(data[9]),
            sijaintitunnus=self._format_unit_id(data[0]),
            rakennus_muoto=self._parse_str(data[21]),
            rakentaminen_aloitettu_pvm=self._parse_date(data[4]),
            rakentaminen_valmis_pvm=self._parse_date(data[5]),
            rakentaminen_valmiusaste=self._parse_int(data[6]),
            peruskorjausvuosi=self._parse_int(data[10]),
            ika_alennus_laskentavuosi=self._parse_int(data[11]),
            viimeistelematon_kellari_pinta_ala=self._parse_int(data[12]),
            kuisti_pinta_ala=self._parse_int(data[18]),
            varasto_pinta_ala=self._parse_int(data[22]),
            hissikuilu_pinta_ala=self._parse_int(data[24]),
            kellari_pinta_ala=self._parse_int(data[27]),
            paikoitusalue_pinta_ala=self._parse_int(data[28]),
            siilo_tyyppi=self._parse_str(data[29]),
            siilo_halkaisija=self._format_numeric(data[30]),
            sahko=self._parse_bool(data[13]),
            lammitys=self._parse_str(data[14]),
            vesijohto=self._parse_bool(data[15]),
            viemari=self._parse_bool(data[16]),
            talviasuttava=self._parse_bool(data[17]),
            vessa=self._parse_bool(data[19]),
            sauna=self._parse_bool(data[20]),
            hissi=self._parse_bool(data[23]),
            ilmastointi=self._parse_int(data[25]),
            lammitys_ja_vesi=self._parse_int(data[26]),
            kasvihuone_rakenne_ja_lammitys=self._parse_str(data[31]),
            kevytrakenteinen_ruokala=self._parse_bool(data[32]),
            pysakointitalo_katettu_ylin_kerros=self._parse_bool(data[33]),
            lampoeristys=self._parse_bool(data[34])
        )


class MuuProfiiliParser(TaxItemParser):
    versions = {19}

    def parse(self, data: list[str], creation_date: date) -> MuuProfiili:
        return MuuProfiili(
            tieto_pvm=creation_date,
            muu_profiili_numero=self._parse_int(data[1]),
            muu_profiili_tyyppi=self._parse_str(data[2]),
            vesivoima_arvo=self._format_numeric(data[3]),
            verotusarvo=self._format_numeric(data[4]),
            kiinteistovero=self._format_numeric(data[5]),
            sijaintitunnus=self._format_unit_id(data[0])
        )
