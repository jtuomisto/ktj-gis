import hashlib
from warnings import warn
from collections.abc import Iterator
from itertools import chain
from datetime import datetime
from lxml.etree import ElementBase as Element, parse as parse_xml
from ktj_gis.misc import element_get_tag, element_get_text
from .ktjcadastralparser import format_unit_id
from ktj_gis.ktjitems import Henkilo, Yhteystieto, Yhteystiedot


def id_hash(id_string: str) -> str:
    return hashlib.sha3_256(bytes(id_string, encoding='utf8')).hexdigest()


class KTJContactParser:
    def __init__(self, root_or_fileobj: Element | object):
        if isinstance(root_or_fileobj, Element):
            self._root = root_or_fileobj
        else:
            self._root = parse_xml(root_or_fileobj).getroot()

        if (tag := element_get_tag(self._root)) != 'Yhteystiedot':
            raise ValueError(f'Invalid root element {tag}')

        date_el = self._root.find('.//{*}metatiedot/{*}tiedostonLuontiaika')
        self._release_date = datetime.fromisoformat(element_get_text(date_el))

    def _name_formatter(self, etunimet: str, sukunimi: str) -> str:
        if etunimet is None and sukunimi is None:
            return 'Tuntematon'

        formatted = []

        if etunimet is not None:
            formatted.append(etunimet)

        if sukunimi is not None:
            formatted.append(sukunimi)

        return ' '.join(formatted)

    def _osoite_formatter(self, osoite: str, postinumero: str, toimipaikka: str) -> str:
        formatted = []

        if osoite is not None:
            formatted.append(osoite)

        if postinumero is not None and toimipaikka is not None:
            formatted.append(f'{postinumero} {toimipaikka}')

        if len(formatted) == 0:
            return 'Tuntematon'

        return ', '.join(formatted)

    def _parse_yhteystiedot(self, el: Element) -> Iterator[Henkilo | Yhteystieto]:
        sijaintitunnus = element_get_text(el, 'maaraalatunnus', coerce=format_unit_id)

        if sijaintitunnus is None:
            sijaintitunnus = element_get_text(el, 'kiinteistotunnus', coerce=format_unit_id)

        relations = []

        for p in el.iterfind('.//{*}Henkilo'):
            etunimet = element_get_text(p, 'etunimet')
            sukunimi = element_get_text(p, 'sukunimi')
            ulkomaalainen = element_get_text(p, 'ulkomaalainen')
            osoite = element_get_text(p, 'jakeluosoite')
            postinumero = element_get_text(p, 'postinumero')
            toimipaikka = element_get_text(p, 'paikkakunta')
            nimi = element_get_text(p, 'nimi')
            y_tunnus = element_get_text(p, 'ytunnus')
            henkilotunnus = element_get_text(p, 'henkilotunnus')
            formatted_name = self._name_formatter(etunimet, sukunimi)
            formatted_osoite = self._osoite_formatter(osoite, postinumero, toimipaikka)
            tunnus = None

            if ulkomaalainen is not None:
                ulkomaalainen = False if ulkomaalainen == 'false' else True

            if y_tunnus is None and henkilotunnus is None:
                if nimi is not None:
                    osoite = formatted_osoite
                    tunnus = id_hash(nimi)
                elif formatted_name != 'Tuntematon':
                    nimi = formatted_name
                    osoite = formatted_osoite
                    tunnus = id_hash(formatted_name)
                else:
                    warn('Skipping contact information, missing too much data')
                    continue
            elif y_tunnus is None:
                if nimi is None:
                    nimi = formatted_name

                osoite = formatted_osoite
                tunnus = id_hash(henkilotunnus)
            else:
                tunnus = id_hash(y_tunnus)

            relations.append(tunnus)

            yield Henkilo(
                tunnus, nimi, self._release_date,
                osoite=osoite,
                y_tunnus=y_tunnus,
                ulkomaalainen=ulkomaalainen
            )

        # Keep contacts for items that have been removed, for now
        #     yield Yhteystieto(sijaintitunnus, None, self._status_date, _deleted=True)
        if len(relations) == 0:
            return

        yield Yhteystiedot(sijaintitunnus, [
            Yhteystieto(sijaintitunnus, tunnus, self._release_date)
            for tunnus in relations
        ])

    @property
    def release_date(self) -> datetime:
        return self._release_date

    def parse(self) -> Iterator[Henkilo | Yhteystiedot]:
        r = self._root

        for el in chain(r.iterfind('{*}Rekisteriyksikko'), r.iterfind('{*}Maaraala')):
            yield from self._parse_yhteystiedot(el)
