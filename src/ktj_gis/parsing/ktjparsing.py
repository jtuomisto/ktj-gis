from collections.abc import Iterator, Iterable
from pathlib import Path
from ktj_gis.ktjitems import KTJItem
from ktj_gis.downloading import KTJFile, KTJFiletype
from . import KTJCadastralParser, KTJContactParser


def parse_ktj_file(file: str | KTJFile) -> Iterator[KTJItem]:
    if isinstance(file, str):
        file = KTJFile.from_existing(file)

    with file.open() as handle:
        p = None

        match file.filetype:
            case KTJFiletype.CADASTRAL:
                p = KTJCadastralParser(handle)
            case KTJFiletype.CONTACT:
                p = KTJContactParser(handle)
            case _:
                raise RuntimeError(f'No known parser for: {file.filename}')

        yield from p.parse()


def parse_ktj_files(files: Iterable[KTJFile]) -> Iterator[Iterator[KTJItem]]:
    for f in sorted(files, key=lambda x: x.updated):
        yield parse_ktj_file(f)


def parse_ktj_files_from_directory(directory: str) -> Iterator[Iterator[KTJItem]]:
    files = []

    for f in Path(directory).iterdir():
        if f.is_dir() or not f.name.lower().endswith('.zip'):
            continue

        files.append(KTJFile.from_existing(f.absolute()))

    yield from parse_ktj_files(files)
