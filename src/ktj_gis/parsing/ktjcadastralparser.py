from collections.abc import Iterator, Iterable
from datetime import datetime, date
from osgeo import osr
from lxml.etree import ElementBase as Element, parse as parse_xml
from ktj_gis.misc import element_get_tag, element_get_text
from ktj_gis.ktjitems import *
from .geometryparsing import parse_ktj_geometry


GML_ID = '{http://www.opengis.net/gml}id'
XML_LANG = '{http://www.w3.org/XML/1998/namespace}lang'
XML_HREF = '{http://www.w3.org/1999/xlink}href'
LANG = 'fi'


def format_unit_id(uid: str) -> str:
    """
    Format ids into more human readable format, eg.
    12301212340123 -> 123-12-1234-123
    02312312340123M0201 -> 23-123-1234-123M201
    """

    if uid is None:
        return None

    parts = map(lambda x: str(int(x)), (uid[0:3], uid[3:6], uid[6:10], uid[10:14]))
    formatted = '-'.join(parts)

    if len(uid) == 19:
        formatted += 'M' + str(int(uid[15:19]))

    return formatted


def date_coercer(date_string: str) -> date:
    """
    Fix erroneous date values

    Older data might contain only a year for a date-field
    """

    if date_string is None:
        return None

    string_len = len(date_string)

    if string_len == 4:
        date_string += '0101'

    return date.fromisoformat(date_string)


class KTJCadastralParser:
    kiinteistorajaviittaus_xpath = './/{*}kiinteistorajat/{*}RekisteriyksikonSuhdeKiinteistorajaan/{*}kiinteistorajaviittaus'
    rajamerkkiviittaus_xpath = './/{*}rajamerkit/{*}RekisteriyksikonSuhdeRajamerkkiin/{*}rajamerkkiviittaus'
    kaavasuhde_xpath = './/{*}kaavat/{*}RekisteriyksikonSuhdeKaavaan'
    palsta_xpath = './/{*}palstat/{*}Palsta'
    rekisteriyksikon_muodostuminen_xpath = './/{*}muodostumistoimenpiteet/{*}RekisteriyksikonSuhdeMuodostumistoimenpiteeseen'
    muodostuminen_rekisteriyksikosta_xpath = './/{*}muodostumisetRekisteriyksikosta/{*}muodostuminenRekisteriyksikosta'
    muodostuminen_maaraalasta_xpath = './/{*}muodostumisetMaaraalasta/{*}muodostuminenMaaraalasta'
    muodostuminen_rekyks_maaraaloista_xpath = './/{*}muodostumisetRekisteriyksikonMaaraaloista/{*}muodostuminenRekisteriyksikonMaaraaloista'
    maaraalansijainti_xpath = './/{*}maaraalaSijaintirekisteriyksikkosuhteet/{*}MaaraalaSijaintirekisteriyksikkosuhde'
    maaraalanosa_xpath = './/{*}maaraalanOsat/{*}MaaraalanOsa'
    kayttooikeusyksikonosa_xpath = './/{*}kayttooikeusyksikonOsat/{*}KayttooikeusyksikonOsa'
    rekisteriyksikko_xpath = './/{*}rekisteriyksikot/{*}Rekisteriyksikko'
    maaraala_xpath = './/{*}maaraalat/{*}Maaraala'
    kayttooikeusyksikko_xpath = './/{*}kayttooikeusyksikot/{*}Kayttooikeusyksikko'
    kiinteistoraja_xpath = './/{*}kiinteistorajat/{*}Kiinteistoraja'
    rajamerkki_xpath = './/{*}rajamerkit/{*}Rajamerkki'
    luontiaika_xpath = './/{*}metatiedot/{*}IrrotuksenTiedot/{*}irrotustilasto/{*}tiedostonLuontiaika'

    def __init__(self, root_or_fileobj: Element | object):
        if isinstance(root_or_fileobj, Element):
            self._root = root_or_fileobj
        else:
            self._root = parse_xml(root_or_fileobj).getroot()

        if (tag := element_get_tag(self._root)) != 'TietoaKiinteistorekisterista':
            raise ValueError(f'Invalid root element {tag}')

        self._srs = osr.SpatialReference()
        self._srs.SetFromUserInput(self._root.get('srsName'))
        self._srs.AutoIdentifyEPSG()

        date_el = self._root.find(self.luontiaika_xpath)
        self._release_date = datetime.fromisoformat(element_get_text(date_el))

    def _parse_fid_from_element(self, el: Element, attr: str=GML_ID) -> int:
        if (id_string := el.get(attr)) is None:
            return None

        fid = ''.join(filter(lambda x: x.isnumeric(), id_string))

        if len(fid) == 0:
            return None

        return int(fid)

    def _parse_rekisteriyksikko(self, el: Element) -> Rekisteriyksikko:
        kitu = element_get_text(el, 'kiinteistotunnus', coerce=format_unit_id)
        olotila = element_get_text(el, 'olotila', coerce=int)
        laji = element_get_text(el, 'rekisteriyksikkolaji', coerce=int)
        peruskiinteisto_suhde = element_get_text(el, 'suhdePeruskiinteistoon', coerce=int)
        rekisterointi_pvm = element_get_text(el, 'rekisterointipvm', coerce=date_coercer)
        lakkaamis_pvm = element_get_text(el, 'lakkaamispvm', coerce=date_coercer)
        kuntatunnus = element_get_text(el, 'kuntatunnus', coerce=int)
        nimi = element_get_text(el, 'nimi')
        pinta_ala_maa = element_get_text(el, 'maapintaala', default='0')
        pinta_ala_vesi = element_get_text(el, 'vesipintaala', default='0')
        kayttotarkoitus = element_get_text(el, 'kayttotarkoituslyhenne')
        kayttotarkoitus_tarkenne = element_get_text(el, f'kayttotarkoitusSelvakielisena[@{XML_LANG}="{LANG}"]')
        osaluku = element_get_text(el, 'osaluku')
        manttaali = element_get_text(el, 'manttaali')
        arkistoviite = element_get_text(el, 'arkistoviite')

        if kayttotarkoitus is not None:
            if kayttotarkoitus_tarkenne is not None:
                kayttotarkoitus = f'{kayttotarkoitus}: {kayttotarkoitus_tarkenne}'

            kayttotarkoitus = kayttotarkoitus.strip('.,-: ')

        return Rekisteriyksikko(
            kitu, olotila, laji,
            peruskiinteisto_suhde,
            kuntatunnus, self._release_date,
            rekisterointi_pvm=rekisterointi_pvm,
            lakkaamis_pvm=lakkaamis_pvm,
            nimi=nimi,
            pinta_ala_maa=pinta_ala_maa,
            pinta_ala_vesi=pinta_ala_vesi,
            kayttotarkoitus=kayttotarkoitus,
            osaluku=osaluku,
            manttaali=manttaali,
            arkistoviite=arkistoviite
        )

    def _parse_rekisteriyksikko_kiinteistorajat(self, el: Element, parent: Rekisteriyksikko) -> Iterator[RekisteriyksikkoKiinteistoraja]:
        kitu = element_get_text(el, 'kiinteistotunnus', coerce=format_unit_id)

        for k in el.iterfind(self.kiinteistorajaviittaus_xpath):
            yield RekisteriyksikkoKiinteistoraja(
                kitu, self._parse_fid_from_element(k, attr=XML_HREF),
                self._release_date, _parent=parent
            )

    def _parse_rekisteriyksikko_rajamerkit(self, el: Element, parent: Rekisteriyksikko) -> Iterator[RekisteriyksikkoRajamerkki]:
        kitu = element_get_text(el, 'kiinteistotunnus', coerce=format_unit_id)

        for r in el.iterfind(self.rajamerkkiviittaus_xpath):
            yield RekisteriyksikkoRajamerkki(
                kitu, self._parse_fid_from_element(r, attr=XML_HREF),
                self._release_date, _parent=parent
            )

    def _parse_rekisteriyksikko_kaavat(self, el: Element, parent: Rekisteriyksikko) -> Iterator[RekisteriyksikkoKaava]:
        kitu = element_get_text(el, 'kiinteistotunnus', coerce=format_unit_id)

        for k in el.iterfind(self.kaavasuhde_xpath):
            kaavatunnus = element_get_text(k, 'kaavatunnus')
            kaavalaji = element_get_text(k, 'kaavalaji', coerce=int)
            hyvaksytty_pvm = element_get_text(k, 'hyvaksymispvmTaiVahvistamispvm', coerce=date_coercer)
            voimaantulo_pvm = element_get_text(k, 'voimaantulopvm', coerce=date_coercer)
            lakkaamis_pvm = element_get_text(k, 'lakkaamispvmRekisteriyksikonKannalta', coerce=date_coercer)
            arkistotunnus = element_get_text(k, 'kaavanArkistotunnus')
            yksilointitunnus = element_get_text(k, 'kaavanYksilointitunnus')
            olotila = element_get_text(k, 'olotila', coerce=int)
            selitys = element_get_text(k, 'selitykset')

            yield RekisteriyksikkoKaava(
                kitu, kaavatunnus, kaavalaji,
                olotila, self._release_date,
                hyvaksytty_pvm=hyvaksytty_pvm,
                voimaantulo_pvm=voimaantulo_pvm,
                lakkaamis_pvm=lakkaamis_pvm,
                arkistotunnus=arkistotunnus,
                yksilointitunnus=yksilointitunnus,
                selitys=selitys,
                _parent=parent
            )

    def _parse_palsta(self, el: Element, parent: Rekisteriyksikko) -> Iterator[Palsta]:
        kitu = element_get_text(el, 'kiinteistotunnus', coerce=format_unit_id)

        for p in el.iterfind(self.palsta_xpath):
            fid = self._parse_fid_from_element(p)
            geom = parse_ktj_geometry(p, self._srs, only_of_type=ogr.wkbPolygon)

            if len(geom) != 1:
                raise ValueError(f'Invalid amount of polygons for a parcel: {len(geom)}')

            yield Palsta(fid, kitu, self._release_date, geom[0], _parent=parent)

    def _parse_muodostuminen(self, el: Element, parent: Rekisteriyksikko) -> Iterator[Muodostuminen | Muodostumistoimenpide]:
        kitu = element_get_text(el, 'kiinteistotunnus', coerce=format_unit_id)

        for c, r in enumerate(el.iterfind(self.rekisteriyksikon_muodostuminen_xpath), start=1):
            tunnus = f'{c}-{kitu}'
            laji = element_get_text(r, 'toimenpidelaji', coerce=int)
            rekisterointi_pvm = element_get_text(r, 'rekisterointipvm', coerce=date_coercer)
            toimitus_pvm = element_get_text(r, 'toimituspvm', coerce=date_coercer)
            muutos_pinta_ala_maa = element_get_text(r, 'maapintaalamuutos', direct_child=True, default='0')
            muutos_pinta_ala_vesi = element_get_text(r, 'vesipintaalamuutos', direct_child=True, default='0')
            selitys = element_get_text(r, 'selitykset')

            if toimitus_pvm is None:
                toimitus_pvm = rekisterointi_pvm

            yield (m := Muodostuminen(
                kitu, tunnus, laji, self._release_date,
                toimitus_pvm=toimitus_pvm,
                muutos_pinta_ala_maa=muutos_pinta_ala_maa,
                muutos_pinta_ala_vesi=muutos_pinta_ala_vesi,
                selitys=selitys,
                _parent=parent
            ))
            yield from self._parse_muodostumistoimenpide(r, m)

    def _parse_muodostumistoimenpide(self, el: Element, parent: Muodostuminen) -> Iterator[Muodostumistoimenpide]:
        properties = {}

        for m in el.iterfind(self.muodostuminen_rekisteriyksikosta_xpath):
            kitu = element_get_text(m, 'kiinteistotunnus', coerce=format_unit_id)
            rekisteriyksikon_lakkaaminen = element_get_text(m, 'rekisteriyksikonLakkaaminenKiinteistojaotuksenMuutoksessa', coerce=int)
            muutos_pinta_ala_maa = element_get_text(m, 'maapintaalamuutos', default='0')
            muutos_pinta_ala_vesi = element_get_text(m, 'vesipintaalamuutos', default='0')

            yield Muodostumistoimenpide(
                parent.tunnus, kitu, rekisteriyksikon_lakkaaminen, self._release_date,
                muutos_pinta_ala_maa=muutos_pinta_ala_maa,
                muutos_pinta_ala_vesi=muutos_pinta_ala_vesi,
                _parent=parent
            )

        for m in el.iterfind(self.muodostuminen_maaraalasta_xpath):
            kitu = element_get_text(m, 'kiinteistotunnus', coerce=format_unit_id)
            rekisteriyksikon_lakkaaminen = element_get_text(m, 'maaraalanSijaintirekisteriyksikonLakkaaminenKiinteistojaotuksenMuutoksessa', coerce=int)
            maara_alan_tunnus = element_get_text(m, 'maaraalatunnus', coerce=format_unit_id)
            maara_alan_lakkaaminen = element_get_text(m, 'maaraalanLakkaaminenKiinteistojaotuksenMuutoksessa', coerce=int)
            muutos_pinta_ala_maa = element_get_text(m, 'maapintaalamuutos', default='0')
            muutos_pinta_ala_vesi = element_get_text(m, 'vesipintaalamuutos', default='0')

            p = properties.setdefault(kitu, set())
            p.add(rekisteriyksikon_lakkaaminen)

            yield Muodostumistoimenpide(
                parent.tunnus, kitu, rekisteriyksikon_lakkaaminen, self._release_date,
                maara_alan_tunnus=maara_alan_tunnus,
                maara_alan_lakkaaminen=maara_alan_lakkaaminen,
                muutos_pinta_ala_maa=muutos_pinta_ala_maa,
                muutos_pinta_ala_vesi=muutos_pinta_ala_vesi,
                _parent=parent
            )

        for m in el.iterfind(self.muodostuminen_rekyks_maaraaloista_xpath):
            kitu = element_get_text(m, 'kiinteistotunnus', coerce=format_unit_id)
            muutos_pinta_ala_maa = element_get_text(m, 'maapintaalamuutos', default='0')
            muutos_pinta_ala_vesi = element_get_text(m, 'vesipintaalamuutos', default='0')
            lakkaaminen = properties.get(kitu)

            if lakkaaminen is None:
                rekisteriyksikon_lakkaaminen = 0
            elif len(lakkaaminen) == 1:
                rekisteriyksikon_lakkaaminen = lakkaaminen.pop()
            else:
                rekisteriyksikon_lakkaaminen = max(lakkaaminen)

            yield Muodostumistoimenpide(
                parent.tunnus, kitu, rekisteriyksikon_lakkaaminen, self._release_date,
                muutos_pinta_ala_maa=muutos_pinta_ala_maa,
                muutos_pinta_ala_vesi=muutos_pinta_ala_vesi,
                rekisteriyksikon_maara_aloista=True,
                _parent=parent
            )

    def _parse_maaraala(self, el: Element) -> MaaraAla:
        tunnus = element_get_text(el, 'maaraalatunnus', coerce=format_unit_id)
        olotila = element_get_text(el, 'olotila', coerce=int, direct_child=True)
        rekisterointi_pvm = element_get_text(el, 'rekisterointipvm', coerce=date_coercer, direct_child=True)
        lakkaamis_pvm = element_get_text(el, 'lakkaamispvm', coerce=date_coercer, direct_child=True)
        maara_alan_olotila = element_get_text(el, 'maaraalanOlotila', coerce=int)
        lakkaamis_syy = element_get_text(el, 'lakkaamissyy', coerce=int, direct_child=True)
        tyyppi = element_get_text(el, 'maaraalanTyyppi', coerce=int)
        peruskiinteisto_suhde = element_get_text(el, 'suhdePeruskiinteistoon', coerce=int)
        saantotapa = element_get_text(el, 'maaraalanSaantotapa', coerce=int)
        tietolahde = element_get_text(el, 'maaraalanTietolahde', coerce=int)
        alkuperainen_saanto_pvm = element_get_text(el, 'alkuperainenSaantopvm', coerce=date_coercer)

        return MaaraAla(
            tunnus, olotila, maara_alan_olotila, lakkaamis_syy,
            tyyppi, peruskiinteisto_suhde, saantotapa, tietolahde,
            alkuperainen_saanto_pvm, self._release_date,
            rekisterointi_pvm=rekisterointi_pvm,
            lakkaamis_pvm=lakkaamis_pvm
        )

    def _parse_maaraalarekisteriyksikko(self, el: Element, parent: MaaraAla) -> MaaraAlaRekisteriyksikko:
        for m in el.iterfind(self.maaraalansijainti_xpath):
            tunnus = element_get_text(m, 'maaraalatunnus', coerce=format_unit_id)
            kitu = element_get_text(m, 'kiinteistotunnus', coerce=format_unit_id)
            olotila = element_get_text(m, 'olotila', coerce=int)
            rekisterointi_pvm = element_get_text(m, 'rekisterointipvm', coerce=date_coercer)
            lakkaamis_syy = element_get_text(m, 'lakkaamissyy', coerce=int)
            lakkaamis_pvm = element_get_text(m, 'lakkaamispvm', coerce=date_coercer)

            yield MaaraAlaRekisteriyksikko(
                tunnus, kitu, olotila,
                lakkaamis_syy, self._release_date,
                rekisterointi_pvm=rekisterointi_pvm,
                lakkaamis_pvm=lakkaamis_pvm,
                _parent=parent
            )

    def _parse_maaraalaosa(self, el: Element, parent: MaaraAla) -> Iterator[MaaraAlaOsa]:
        tunnus = element_get_text(el, 'maaraalatunnus', coerce=format_unit_id)

        for m in el.iterfind(self.maaraalanosa_xpath):
            fid = self._parse_fid_from_element(m)
            geom = parse_ktj_geometry(m, self._srs, only_of_type=ogr.wkbPoint)

            if len(geom) != 1:
                raise ValueError(f'Invalid amount of points for a unit: {len(geom)}')

            yield MaaraAlaOsa(fid, tunnus, self._release_date, geom[0], _parent=parent)

    def _parse_kayttooikeusyksikko(self, el: Element) -> Kayttooikeusyksikko:
        tunnus = element_get_text(el, 'kayttooikeusyksikkotunnus')
        olotila = element_get_text(el, 'olotila', coerce=int)
        laji = element_get_text(el, 'kayttooikeusyksikkolaji', coerce=int)
        rekisterointi_pvm = element_get_text(el, 'rekisterointipvm', coerce=date_coercer)
        nimi = element_get_text(el, 'nimi')

        return Kayttooikeusyksikko(
            tunnus, olotila, laji, self._release_date,
            rekisterointi_pvm=rekisterointi_pvm,
            nimi=nimi
        )

    def _parse_kayttooikeusyksikkoosa(self, el: Element, parent: Kayttooikeusyksikko) -> Iterator[KayttooikeusyksikkoOsa]:
        tunnus = element_get_text(el, 'kayttooikeusyksikkotunnus')

        for k in el.iterfind(self.kayttooikeusyksikonosa_xpath):
            fid = self._parse_fid_from_element(k)
            numero = element_get_text(k, 'kayttooikeusyksikonOsanNumero', coerce=int)
            olotila = element_get_text(el, 'olotila', coerce=int)
            rekisterointi_pvm = element_get_text(el, 'rekisterointipvm', coerce=date_coercer)
            geom = parse_ktj_geometry(k, self._srs, linearize=True)

            if len(geom) == 0:
                # These are allowed to have no geometry
                continue

            yield KayttooikeusyksikkoOsa(
                fid, tunnus, numero, olotila, self._release_date,
                geom[-1], # Always contains a point in addition to actual geometry
                rekisterointi_pvm=rekisterointi_pvm,
                _parent=parent
            )

    def _parse_kiinteistoraja(self, el: Element) -> Kiinteistoraja:
        fid = self._parse_fid_from_element(el)
        laji = element_get_text(el, 'kiinteistorajalaji', coerce=int)
        lahdeaineisto = element_get_text(el, 'lahdeaineisto', coerce=int)
        luonne1 = element_get_text(el, 'luonne1', coerce=int)
        luonne2 = element_get_text(el, 'luonne2', coerce=int)
        rekisterointi_pvm = element_get_text(el, 'rekisterointipvm', coerce=date_coercer)
        geom = parse_ktj_geometry(el, self._srs, only_of_type=ogr.wkbCompoundCurve)

        if len(geom) != 1:
            raise ValueError(f'Invalid amount of geometries for a border: {len(geom)}')

        return Kiinteistoraja(
            fid, laji, lahdeaineisto, luonne1, luonne2,
            self._release_date, geom[0],
            rekisterointi_pvm=rekisterointi_pvm
        )

    def _parse_rajamerkki(self, el: Element) -> Rajamerkki:
        fid = self._parse_fid_from_element(el)
        laji = element_get_text(el, 'rajamerkkilaji', coerce=int)
        rakenne = element_get_text(el, 'rakenne', coerce=int)
        lahdeaineisto = element_get_text(el, 'lahdeaineisto', coerce=int)
        maanpinta_suhde = element_get_text(el, 'suhdeMaanpintaan', coerce=int)
        olemassaolo = element_get_text(el, 'olemassaolo', coerce=int)
        rekisterointi_pvm = element_get_text(el, 'rekisterointipvm', coerce=date_coercer)
        numero = element_get_text(el, 'numero')
        sijaintitarkkuus = element_get_text(el, 'tasosijaintitarkkuus')
        geom = parse_ktj_geometry(el, self._srs, only_of_type=ogr.wkbPoint)

        if len(geom) != 1:
            raise ValueError(f'Invalid amount of geometries for a border marker: {len(geom)}')

        return Rajamerkki(
            fid, laji, rakenne, lahdeaineisto, maanpinta_suhde,
            olemassaolo, self._release_date, geom[0],
            rekisterointi_pvm=rekisterointi_pvm,
            numero=numero,
            sijaintitarkkuus=sijaintitarkkuus
        )

    @property
    def release_date(self) -> datetime:
        return self._release_date

    def parse(self) -> Iterator[KTJItem]:
        rekisteriyksikot = self._root.iterfind(self.rekisteriyksikko_xpath)
        maaraalat = self._root.iterfind(self.maaraala_xpath)
        kayttooikeusyksikot = self._root.iterfind(self.kayttooikeusyksikko_xpath)
        kiinteistorajat = self._root.iterfind(self.kiinteistoraja_xpath)
        rajamerkit = self._root.iterfind(self.rajamerkki_xpath)

        for el in rekisteriyksikot:
            yield (p := self._parse_rekisteriyksikko(el))
            yield from self._parse_palsta(el, p)
            yield from self._parse_muodostuminen(el, p)
            yield from self._parse_rekisteriyksikko_kiinteistorajat(el, p)
            yield from self._parse_rekisteriyksikko_rajamerkit(el, p)
            yield from self._parse_rekisteriyksikko_kaavat(el, p)

        for el in maaraalat:
            yield (p := self._parse_maaraala(el))
            yield from self._parse_maaraalarekisteriyksikko(el, p)
            yield from self._parse_maaraalaosa(el, p)

        for el in kayttooikeusyksikot:
            yield (p := self._parse_kayttooikeusyksikko(el))
            yield from self._parse_kayttooikeusyksikkoosa(el, p)

        for el in kiinteistorajat:
            yield self._parse_kiinteistoraja(el)

        for el in rajamerkit:
            yield self._parse_rajamerkki(el)
