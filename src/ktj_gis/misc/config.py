from __future__ import annotations
from typing import Any, Hashable
import tomllib


class Config(dict):
    @staticmethod
    def create_from_file(filename: str) -> Config:
        with open(filename, mode='rb') as handle:
            return Config(tomllib.load(handle))

    def get_postgres_uri(self) -> str:
        pg = self.getmany(
            'postgres_user', 'postgres_password','postgres_host',
            'postgres_port', 'postgres_db'
        )

        if any(map(lambda x: x is None, pg)):
            raise KeyError('Missing config keys for postgres')

        return 'postgresql://{}:{}@{}:{}/{}'.format(*pg)

    def get_auth(self, username_key: str, password_key: str) -> tuple[str, str]:
        user, passw = self.getmany(username_key, password_key)

        if user is None or passw is None:
            raise KeyError('Missing username/password')

        return user, passw

    def get(self, key: Hashable, default: Any=None, error_on_missing: bool=False) -> Any:
        value = super().get(key)

        if (is_str := isinstance(value, str)):
            value = value.strip()

        if value is None or (is_str and len(value) == 0):
            if error_on_missing:
                raise KeyError(f'Missing config key: {key}')

            return default

        return value

    def getmany(self, *args) -> list[Any]:
        return [self.get(k) for k in args]
