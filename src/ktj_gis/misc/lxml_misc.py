from typing import Any, Callable
import lxml.etree as XML


def element_get_tag(element: XML.Element) -> str:
    return XML.QName(element).localname


def element_get_text(element: XML.Element, child_tag: str=None,
                     direct_child: bool=False, default: Any=None,
                     coerce: Callable=lambda x: x) -> Any:
    el = element

    if child_tag is not None:
        path = ['{*}', child_tag]

        if not direct_child:
            path.insert(0, './/')

        el = element.find(''.join(path))

    if el is None or el.text is None:
        return default

    text = el.text.strip()

    if len(text) == 0:
        return default

    return coerce(text)
