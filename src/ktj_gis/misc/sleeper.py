from threading import Event
from datetime import datetime


class Sleeper:
    SLEEP_TIME = 60*60 # Sleep for one hour at a time

    def __init__(self):
        self._ev = Event()

    def wake(self):
        self._ev.set()

    def sleep_until(self, until: datetime):
        delta = until - datetime.now()
        self._ev.clear()

        while (d := delta.total_seconds()) > 0:
            if self._ev.is_set():
                break

            self._ev.wait(self.SLEEP_TIME if d > self.SLEEP_TIME else d)
            delta = until - datetime.now()
