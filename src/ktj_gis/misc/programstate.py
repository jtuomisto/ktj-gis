from typing import Any
import json
from datetime import date, datetime
from os.path import exists as file_exists


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if not isinstance(o, date):
            return super().default(o)

        if not isinstance(o, datetime):
            dt = datetime(o.year, o.month, o.day)
        else:
            dt = o

        return {'__dt__': dt.isoformat()}


def custom_hook(obj: dict) -> Any:
    if (dt := obj.get('__dt__')) is None:
        return obj

    return datetime.fromisoformat(dt)


class ProgramState:
    INDENT = 2

    def __init__(self, file: str):
        self._file = file
        self._state = {}

        if file_exists(self._file):
            self._load_state()

    def _load_state(self):
        with open(self._file, mode='rt') as handle:
            self._state = json.load(handle, object_hook=custom_hook)

    def _save_state(self):
        with open(self._file, mode='wt') as handle:
            json.dump(self._state, handle, indent=self.INDENT, cls=CustomEncoder)

    def update(self, __state: dict=None, **kwargs):
        if __state is not None:
            self._state.update(__state)

        self._state.update(kwargs)
        self._save_state()

    def get(self, key: str, default: Any=None) -> Any:
        return self._state.get(key, default)

    def remove(self, key: str) -> Any:
        self._state.pop(key)
        self._save_state()
