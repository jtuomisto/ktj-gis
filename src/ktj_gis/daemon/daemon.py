from __future__ import annotations
import signal
from collections.abc import Callable
from os import rmdir, name as os_name
from os.path import join as path_join, normpath
from tempfile import mkdtemp
from datetime import datetime, timedelta, time
from warnings import warn
from enum import Enum
from psycopg.errors import DatabaseError
from ktj_gis.misc import Config, ProgramState, Sleeper
from ktj_gis.parsing import parse_ktj_files
from ktj_gis.postgres import KTJDatabase
from ktj_gis.downloading import KTJDownloader, KTJFile, KTJFiletype
from .fileoperations import FileOperations


STATE = {
    'sleep': Sleeper(),
    'run': True
}


def signal_received(*args):
    STATE['run'] = False
    STATE['sleep'].wake()


class UpdateSchedule(Enum):
    DAILY = 1
    WEEKLY = 2

    @staticmethod
    def as_delta(us: UpdateSchedule) -> timedelta:
        match us:
            case UpdateSchedule.DAILY:
                return timedelta(days=1)
            case UpdateSchedule.WEEKLY:
                return timedelta(days=7)


class Daemon:
    MAX_FILE_AGE = timedelta(days=60)
    SCHEDULED_TIME = time(hour=4, minute=0)

    def __init__(self, database: KTJDatabase, downloader: KTJDownloader, data_dir: str):
        self._database = database
        self._downloader = downloader
        self._data_dir = normpath(data_dir)
        self._file_op = FileOperations(self._data_dir)
        self._state = ProgramState(path_join(self._data_dir, 'state.json'))
        self._schedule = UpdateSchedule.DAILY
        self._pre_update_cb = lambda: None
        self._post_update_cb = lambda: None

    def _dir_prefix(self, filetype: KTJFiletype, full: bool) -> str:
        tp = 'full' if full else 'changed'
        ft = 'cadastral' if filetype is KTJFiletype.CADASTRAL else 'contact'
        return f'{tp}_{ft}_'

    def _set_updated(self, filetype: KTJFiletype, updated: datetime):
        if filetype is KTJFiletype.CADASTRAL:
            self._state.update(last_updated_cadastral=updated)
        elif filetype is KTJFiletype.CONTACT:
            self._state.update(last_updated_contact=updated)

    def _get_updated(self, filetype: KTJFiletype):
        if filetype is KTJFiletype.CADASTRAL:
            return self._state.get('last_updated_cadastral')
        elif filetype is KTJFiletype.CONTACT:
            return self._state.get('last_updated_contact')

    def _do_update(self, filetype: KTJFiletype, files: list[KTJFile]):
        with self._database.connect() as db:
            for items in parse_ktj_files(files):
                db.insertmany(items)

        newest = max(files, key=lambda x: x.updated)
        self._set_updated(filetype, newest.updated)

    def _update_full(self, filetype: KTJFiletype):
        prefix = self._dir_prefix(filetype, True)

        with self._file_op.temp_dir(prefix) as tmp:
            files = self._downloader.download_full(tmp.path, only=filetype)

            if len(files) == 0:
                raise RuntimeError(f'No full releases available for {filetype}')

        self._do_update(filetype, files)

    def _update_changes(self, filetype: KTJFiletype):
        after = self._get_updated(filetype)

        if after is None:
            return warn(f'Missing last updated date, can\'t download changes')

        prefix = self._dir_prefix(filetype, False)

        with self._file_op.temp_dir(prefix) as tmp:
            files = self._downloader.download_changes(tmp.path, after, only=filetype)

            if len(files) == 0:
                return tmp.rmdir()

        self._do_update(filetype, files)

    def schedule(self, sched: UpdateSchedule) -> Daemon:
        self._schedule = sched
        return self

    def pre_update_callback(self, callback: Callable) -> Daemon:
        self._pre_update_cb = callback
        return self

    def post_update_callback(self, callback: Callable) -> Daemon:
        self._post_update_cb = callback
        return self

    def run(self):
        cadastral_update = self._get_updated(KTJFiletype.CADASTRAL) is None
        contact_update = self._get_updated(KTJFiletype.CONTACT) is None

        if os_name == 'nt':
            signal.signal(signal.SIGTERM, signal.SIG_DFL)
            signal.signal(signal.SIGINT, signal.SIG_DFL)
        else:
            signal.signal(signal.SIGTERM, signal_received)
            signal.signal(signal.SIGINT, signal_received)

        if cadastral_update or contact_update:
            self._pre_update_cb()

        if cadastral_update:
            self._update_full(KTJFiletype.CADASTRAL)

        if contact_update:
            self._update_full(KTJFiletype.CONTACT)

        delta = UpdateSchedule.as_delta(self._schedule)

        while STATE['run']:
            if not cadastral_update and not contact_update:
                self._pre_update_cb()

            cadastral_update = contact_update = False

            while STATE['run']:
                try:
                    self._update_changes(KTJFiletype.CADASTRAL)
                    self._update_changes(KTJFiletype.CONTACT)
                    break
                except (OSError, RuntimeError, DatabaseError) as e:
                    warn(f'Failure during update: {e!s}')
                    STATE['sleep'].sleep_until(datetime.now() + timedelta(hours=1))

            if STATE['run']:
                self._file_op.temp_cleanup(self.MAX_FILE_AGE)
                self._post_update_cb()

                until = datetime.combine(datetime.now() + delta, self.SCHEDULED_TIME)
                STATE['sleep'].sleep_until(until)
