from tempfile import mkdtemp
from datetime import datetime, timedelta
from shutil import rmtree
from os import rmdir
from os.path import normpath
from pathlib import Path


class TempDirContext:
    def __init__(self, parent: str, prefix: str, keep: bool):
        self._dir = parent
        self._prefix = prefix
        self._keep = keep
        self._tmpdir = None

    def __enter__(self):
        self._tmpdir = mkdtemp(prefix=self._prefix, dir=self._dir)
        return self

    def __exit__(self, exc_type, *args):
        if self._tmpdir is None or (exc_type is None and self._keep):
            return

        try:
            rmtree(self._tmpdir)
        except (FileNotFoundError, OSError):
            # Suppress errors related to deletion
            warn(f'Could not delete temp directory: {self._tmpdir}')

    @property
    def path(self) -> str:
        return self._tmpdir

    def rmdir(self):
        if self._tmpdir is None:
            return

        rmtree(self._tmpdir)
        self._tmpdir = None


class FileOperations:
    def __init__(self, directory: str):
        self._dir = normpath(directory)

    def temp_dir(self, prefix: str, keep: bool=True) -> TempDirContext:
        pre = f'ktjgis_{datetime.now().strftime('%Y%m%d')}_{prefix}'
        return TempDirContext(self._dir, pre, keep)

    def temp_cleanup(self, max_age: timedelta):
        now = datetime.now()

        for p in Path(self._dir).iterdir():
            if p.is_symlink() or not p.is_dir() or not p.name.startswith('ktjgis_'):
                continue

            try:
                _, filedate, *_ = p.name.split('_')
                filedate = datetime.fromisoformat(filedate)
            except (TypeError, ValueError):
                continue

            if now - filedate > max_age:
                rmtree(p.absolute(), ignore_errors=True)
