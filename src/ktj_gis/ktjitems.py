from __future__ import annotations
from collections.abc import Iterable
from dataclasses import dataclass, field
from datetime import datetime, date
from osgeo import ogr


@dataclass
class KTJItem:
    _parent: KTJItem = field(default=None, kw_only=True, repr=False, compare=False)
    _modified: bool = field(default=False, kw_only=True, repr=False, compare=False)
    _deleted: bool = field(default=False, kw_only=True, repr=False, compare=False)


@dataclass
class Rekisteriyksikko(KTJItem):
    """
    <rekisteriyksikot>
        <Rekisteriyksikko>
    """
    kiinteistotunnus: str
    olotila: int
    laji: int
    peruskiinteisto_suhde: int
    kuntatunnus: int
    tieto_pvm: datetime
    rekisterointi_pvm: date = None
    lakkaamis_pvm: date = None
    nimi: str = None
    pinta_ala_maa: str = '0'
    pinta_ala_vesi: str = '0'
    kayttotarkoitus: str = None
    osaluku: str = None
    manttaali: str = None
    arkistoviite: str = None


@dataclass
class RekisteriyksikkoKiinteistoraja(KTJItem):
    """
    <rekisteriyksikot>
        <Rekisteriyksikko>
            <kiinteistorajat>
                <RekisteriyksikonSuhdeKiinteistorajaan>
                    <kiinteistorajaviittaus>
    """
    kiinteistotunnus: str
    kiinteistoraja_fid: int
    tieto_pvm: datetime


@dataclass
class RekisteriyksikkoRajamerkki(KTJItem):
    """
    <rekisteriyksikot>
        <Rekisteriyksikko>
            <rajamerkit>
                <RekisteriyksikonSuhdeRajamerkkiin>
                    <rajamerkkiviittaus>
    """
    kiinteistotunnus: str
    rajamerkki_fid: int
    tieto_pvm: datetime


@dataclass
class RekisteriyksikkoKaava(KTJItem):
    """
    <rekisteriyksikot>
        <Rekisteriyksikko>
            <kaavat>
                <RekisteriyksikonSuhdeKaavaan>
    """
    kiinteistotunnus: str
    kaavatunnus: str
    kaavalaji: int
    olotila: int
    tieto_pvm: datetime
    hyvaksytty_pvm: date = None
    voimaantulo_pvm: date = None
    lakkaamis_pvm: date = None
    arkistotunnus: str = None
    yksilointitunnus: str = None
    selitys: str = None


@dataclass
class Palsta(KTJItem):
    """
    <rekisteriyksikot>
        <Rekisteriyksikko>
            <palstat>
                <Palsta>
    """
    fid: int
    kiinteistotunnus: str
    tieto_pvm: datetime
    geom: ogr.Geometry


@dataclass
class Muodostuminen(KTJItem):
    """
    <rekisteriyksikot>
        <Rekisteriyksikko>
            <muodostumistoimenpiteet>
                <RekisteriyksikonSuhdeMuodostumistoimenpiteeseen>
    """
    kiinteistotunnus: str
    tunnus: str
    laji: int
    tieto_pvm: datetime
    toimitus_pvm: date = None
    muutos_pinta_ala_maa: str = '0'
    muutos_pinta_ala_vesi: str = '0'
    selitys: str = None


@dataclass
class Muodostumistoimenpide(KTJItem):
    """
    <rekisteriyksikot>
        <Rekisteriyksikko>
            <muodostumistoimenpiteet>
                <RekisteriyksikonSuhdeMuodostumistoimenpiteeseen>
                    <muodostumisetRekisteriyksikosta|muodostumisetMaaraalasta>
                        <muodostuminenRekisteriyksikosta|muodostuminenMaaraalasta>
    """
    tunnus: str
    kiinteistotunnus: str
    rekisteriyksikon_lakkaaminen: int
    tieto_pvm: datetime
    maara_alan_tunnus: str = None
    maara_alan_lakkaaminen: int = None
    muutos_pinta_ala_maa: str = '0'
    muutos_pinta_ala_vesi: str = '0'
    rekisteriyksikon_maara_aloista: bool = False


@dataclass
class MaaraAla(KTJItem):
    """
    <maaraalat>
        <Maaraala>
    """
    tunnus: str
    olotila: int
    maara_alan_olotila: int
    lakkaamis_syy: int
    tyyppi: int
    peruskiinteisto_suhde: int
    saantotapa: int
    tietolahde: int
    alkuperainen_saanto_pvm: date
    tieto_pvm: datetime
    rekisterointi_pvm: date = None
    lakkaamis_pvm: date = None


@dataclass
class MaaraAlaRekisteriyksikko(KTJItem):
    """
    <maaraalat>
        <Maaraala>
            <maaraalaSijaintirekisteriyksikkosuhteet>
                <MaaraalaSijaintirekisteriyksikkosuhde>
    """
    tunnus: str
    kiinteistotunnus: str
    olotila: int
    lakkaamis_syy: int
    tieto_pvm: datetime
    rekisterointi_pvm: date = None
    lakkaamis_pvm: date = None


@dataclass
class MaaraAlaOsa(KTJItem):
    """
    <maaraalat>
        <Maaraala>
            <maaraalanOsat>
                <MaaraalanOsa>
    """
    fid: int
    tunnus: str
    tieto_pvm: datetime
    geom: ogr.Geometry


@dataclass
class Kayttooikeusyksikko(KTJItem):
    """
    <kayttooikeusyksikot>
        <Kayttooikeusyksikko>
    """
    tunnus: str
    olotila: int
    laji: int
    tieto_pvm: datetime
    rekisterointi_pvm: date = None
    nimi: str = None


@dataclass
class KayttooikeusyksikkoOsa(KTJItem):
    """
    <kayttooikeusyksikot>
        <Kayttooikeusyksikko>
            <kayttooikeusyksikonOsat>
                <KayttooikeusyksikonOsa>
    """
    fid: int
    tunnus: str
    numero: int
    olotila: int
    tieto_pvm: datetime
    geom: ogr.Geometry
    rekisterointi_pvm: date = None


@dataclass
class Kiinteistoraja(KTJItem):
    """
    <kiinteistorajat>
        <Kiinteistoraja>
    """
    fid: int
    laji: int
    lahdeaineisto: int
    luonne1: int
    luonne2: int
    tieto_pvm: datetime
    geom: ogr.Geometry
    rekisterointi_pvm: date = None


@dataclass
class Rajamerkki(KTJItem):
    """
    <rajamerkit>
        <Rajamerkki>
    """
    fid: int
    laji: int
    rakenne: int
    lahdeaineisto: int
    maanpinta_suhde: int
    olemassaolo: int
    tieto_pvm: datetime
    geom: ogr.Geometry
    rekisterointi_pvm: date = None
    numero: str = None
    sijaintitarkkuus: str = None


@dataclass
class Henkilo(KTJItem):
    """
    <Rekisteriyksikko|Maaraala>
        <kohteenHenkilot>
            <Henkilo>
    """
    tunnus: str
    nimi: str
    tieto_pvm: datetime
    osoite: str = None
    y_tunnus: str = None
    ulkomaalainen: bool = None


@dataclass
class Yhteystieto(KTJItem):
    """
    <Rekisteriyksikko|Maaraala>
    """
    sijaintitunnus: str
    henkilon_tunnus: str
    tieto_pvm: datetime


@dataclass
class Yhteystiedot(KTJItem):
    """Collection of Yhteystieto"""
    sijaintitunnus: str
    yhteystiedot: list[Yhteystieto]
