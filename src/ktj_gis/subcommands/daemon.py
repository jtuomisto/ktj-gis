from time import perf_counter
from argparse import Namespace
from ktj_gis.misc import Config
from ktj_gis.daemon import Daemon, UpdateSchedule
from .subcommand import (get_ktj_database_from_config, get_downloader_from_config,
                         get_parcel_takeover_database_from_config)


def daemon(args: Namespace):
    cfg = Config.create_from_file(args.config)
    downloader = get_downloader_from_config(cfg)
    ktjdb = get_ktj_database_from_config(cfg)
    ptdb = get_parcel_takeover_database_from_config(cfg)
    d = Daemon(ktjdb, downloader, args.data_dir)
    sched = UpdateSchedule.DAILY if args.daily else UpdateSchedule.WEEKLY
    perf = {'counter': 0}

    def pre_update():
        if not args.no_progress:
            print('Update started...')
            perf['counter'] = perf_counter()

    def post_update():
        if args.takeover:
            with ptdb.connect() as db:
                db.update()

        if not args.no_progress:
            print(f'Update took {perf_counter() - perf["counter"]:.3f} seconds.')

    d.schedule(sched).pre_update_callback(pre_update).post_update_callback(post_update)
    d.run()
    print('Quitting...')
