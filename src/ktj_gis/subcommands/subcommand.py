from ktj_gis.misc import Config
from ktj_gis.postgres import KTJDatabase, ParcelTakeoverDatabase, TaxDatabase
from ktj_gis.downloading import KTJDownloader


def get_ktj_database_from_config(cfg: Config) -> KTJDatabase:
    epsg = cfg.get('postgres_epsg', error_on_missing=True)
    schema = cfg.get('ktj_schema', error_on_missing=True)
    return KTJDatabase(cfg.get_postgres_uri(), epsg_number=epsg, schema=schema)


def get_downloader_from_config(cfg: Config) -> KTJDownloader:
    ktj_id = cfg.get('ktj_id', error_on_missing=True)
    ktj_auth = cfg.get_auth('ktj_user', 'ktj_password')
    return KTJDownloader(ktj_id, ktj_auth)


def get_parcel_takeover_database_from_config(cfg: Config) -> ParcelTakeoverDatabase:
    epsg = cfg.get('postgres_epsg', error_on_missing=True)
    schema = cfg.get('ktj_schema', error_on_missing=True)
    pattern = cfg.get('takeover_pattern', error_on_missing=True)
    return ParcelTakeoverDatabase(cfg.get_postgres_uri(), epsg_number=epsg, schema=schema, pattern=pattern)


def get_tax_database_from_config(cfg: Config) -> TaxDatabase:
    schema = cfg.get('tax_schema', error_on_missing=True)
    return TaxDatabase(cfg.get_postgres_uri(), schema=schema)
