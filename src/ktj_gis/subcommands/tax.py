from argparse import Namespace
from collections.abc import Callable
from ktj_gis.misc import Config
from ktj_gis.parsing import TaxCSVParser
from .subcommand import get_tax_database_from_config


def tax(args: Namespace, progress: Callable=None):
    cfg = Config.create_from_file(args.config)
    taxdb = get_tax_database_from_config(cfg)
    taxparser = TaxCSVParser(args.path)

    with taxdb.connect() as db:
        db.insertmany(taxparser.parse(), force=args.force, progress=progress)
