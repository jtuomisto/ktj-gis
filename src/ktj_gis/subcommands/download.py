from collections.abc import Callable
from argparse import Namespace
from ktj_gis.misc import Config
from ktj_gis.downloading import KTJFiletype
from .subcommand import get_downloader_from_config


def download(args: Namespace, progress: Callable=None):
    cfg = Config.create_from_file(args.config)
    dl = get_downloader_from_config(cfg)
    result = []

    if args.cadastral:
        only_type = KTJFiletype.CADASTRAL
    elif args.contact:
        only_type = KTJFiletype.CONTACT
    else:
        only_type = None

    if args.full:
        result = dl.download_full(args.output, progress, only=only_type, dry_run=args.dry_run)
    elif args.changes:
        result = dl.download_changes(args.output, args.after, progress, only=only_type, dry_run=args.dry_run)

    if len(result) == 0:
        print('No files to download')
    elif args.dry_run:
        print('Files that would be downloaded:')
        print('\n'.join([r.filename for r in result]))
