from collections.abc import Callable
from os.path import isdir
from argparse import Namespace
from ktj_gis.misc import Config
from ktj_gis.parsing import parse_ktj_file, parse_ktj_files_from_directory
from .subcommand import get_ktj_database_from_config


def read_xml(args: Namespace, progress: Callable=None):
    cfg = Config.create_from_file(args.config)
    ktjdb = get_ktj_database_from_config(cfg)

    with ktjdb.connect() as db:
        for p in args.paths:
            print(f'Reading from {p}...')

            if isdir(p):
                batches = parse_ktj_files_from_directory(p)
            else:
                batches = [parse_ktj_file(p)]

            for items in batches:
                db.insertmany(items, progress=progress, only_types=args.types)
