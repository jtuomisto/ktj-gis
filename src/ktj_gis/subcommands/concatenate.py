from collections.abc import Callable
from argparse import Namespace
from ktj_gis.modification import KTJConcatenator


def concatenate(args: Namespace, progress: Callable=None):
    concat = KTJConcatenator(args.path, args.output, args.millimeters)
    concat.concate_files(progress)
