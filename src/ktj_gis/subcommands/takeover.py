from collections.abc import Callable
from argparse import Namespace
from ktj_gis.misc import Config
from .subcommand import get_parcel_takeover_database_from_config


def takeover(args: Namespace, progress: Callable=None):
    cfg = Config.create_from_file(args.config)
    ptdb = get_parcel_takeover_database_from_config(cfg)

    with ptdb.connect() as db:
        db.update(progress, no_contacts=args.first_run)
