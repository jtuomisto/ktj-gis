from .concatenate import concatenate as cli_concatenate
from .database import database as cli_database
from .download import download as cli_download
from .takeover import takeover as cli_takeover
from .read_xml import read_xml as cli_read_xml
from .daemon import daemon as cli_daemon
from .tax import tax as cli_tax
