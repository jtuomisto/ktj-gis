from argparse import Namespace
from ktj_gis.misc import Config
from .subcommand import (get_ktj_database_from_config, get_tax_database_from_config,
                         get_parcel_takeover_database_from_config)


def database(args: Namespace):
    cfg = Config.create_from_file(args.config)

    if args.ktj:
        ktjdb = get_ktj_database_from_config(cfg)

        with ktjdb.connect() as db:
            if args.create_tables:
                db.create_tables()

            if args.create_views:
                db.create_views()

            if args.import_definitions:
                db.import_definitions()

    if args.takeover:
        ptdb = get_parcel_takeover_database_from_config(cfg)

        with ptdb.connect() as db:
            if args.create_tables:
                db.create_tables()

            if args.create_views:
                db.create_views()

    if args.tax:
        taxdb = get_tax_database_from_config(cfg)

        with taxdb.connect() as db:
            if args.create_tables:
                db.create_tables()

            if args.create_views:
                db.create_views()

            if args.import_definitions:
                db.import_definitions()
