from warnings import warn
from datetime import datetime, UTC
from urllib.parse import urljoin
from typing import Iterator, Callable
import requests
from bs4 import BeautifulSoup
from .ktjfile import KTJFile, KTJFiletype


class KTJDownloader:
    URL_CADASTRAL = 'https://ws.nls.fi/aineistopalvelu/{ktj_id}/ktjaineistoluovutus/'
    URL_CONTACT = 'https://ws.nls.fi/aineistopalvelu/{ktj_id}/ktjkiraineistoluovutus/'
    MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    def __init__(self, ktj_id: str, auth: tuple[str, str]):
        self._url_cadastral = self.URL_CADASTRAL.format(ktj_id=ktj_id)
        self._url_contacts = self.URL_CONTACT.format(ktj_id=ktj_id)
        self._auth = auth

    def _parse_date(self, dt: str) -> datetime:
        # Dates are formatted like:
        # Sun, 27 Oct 2024 23:10:01 GMT
        parts = dt.split(' ')
        parts[2] = f'{self.MONTHS.index(parts[2]) + 1}'
        return datetime.strptime(' '.join(parts[1:]), '%d %m %Y %H:%M:%S %Z').replace(tzinfo=UTC)

    def _parse_files(self, base_url: str, directory: str, allow_fail: bool=False) -> Iterator[KTJFile]:
        req = requests.get(base_url, auth=self._auth)

        if req.status_code >= 400:
            if not allow_fail:
                raise RuntimeError(f'Failed request: {base_url}, code: {req.status_code}')

            return warn(f'Failed request to {base_url}')

        parser = BeautifulSoup(req.text, 'lxml')

        # First tr is a header
        for row in parser.find_all('tr')[1:]:
            href = row.contents[0].a.get('href')
            _, _, filename = href.rpartition('/')
            updated = self._parse_date(row.contents[-1].string)

            yield KTJFile(filename, urljoin(base_url, href), directory, updated)

    def _parse_all_files(self, directory: str) -> Iterator[KTJFile]:
        yield from self._parse_files(self._url_cadastral, directory, allow_fail=False)
        yield from self._parse_files(self._url_contacts, directory, allow_fail=True)

    def download_full(self, directory: str, progress: Callable=None, only: KTJFiletype=None, dry_run: bool=False) -> list[str]:
        if progress is None:
            progress = lambda: None

        files = {}
        downloaded = []

        for file in self._parse_all_files(directory):
            if file.change or (only is not None and file.filetype is not only):
                continue

            if file.group not in files or file.updated > files[file.group].updated:
                files[file.group] = file

        for file in files.values():
            if not dry_run:
                file.download(self._auth)
                progress()

            downloaded.append(file)

        return downloaded

    def download_changes(self, directory: str, after: datetime, progress: Callable=None, only: KTJFiletype=None, dry_run: bool=False) -> list[str]:
        if progress is None:
            progress = lambda: None

        after_timestamp = 0 if after is None else after.timestamp()
        downloaded = []

        for file in self._parse_all_files(directory):
            if only is not None and file.filetype is not only:
                continue

            if not file.change or file.updated.timestamp() <= after_timestamp:
                continue

            if not dry_run:
                file.download(self._auth)
                progress()

            downloaded.append(file)

        return downloaded
