from __future__ import annotations
from typing import IO
from enum import Enum
from dataclasses import dataclass, KW_ONLY
from os.path import join as path_join, normpath, splitext, basename, dirname
from datetime import datetime, UTC
from zipfile import ZipFile
import requests


class KTJFiletype(Enum):
    CADASTRAL = 1
    CONTACT = 2


class KTJZipFile:
    def __init__(self, file: KTJFile):
        self._file = file
        self._zipfile = None
        self._datafile = None

    def __enter__(self) -> IO:
        self._zipfile = ZipFile(self._file.path, 'r')

        if self._file.filetype is KTJFiletype.CADASTRAL:
            name = splitext(self._file.filename)[0] + '.xml'
        elif self._file.filetype is KTJFiletype.CONTACT:
            name = 'yhteystiedot.xml'
        else:
            self._zipfile.close()
            raise ValueError('Unknown filetype')

        try:
            self._datafile = self._zipfile.open(name, 'r')
        except Exception as e:
            self._zipfile.close()
            raise e

        return self._datafile

    def __exit__(self, *args):
        if self._zipfile is not None:
            self._zipfile.close()

        if self._datafile is not None:
            self._datafile.close()


@dataclass
class KTJFile:
    filename: str
    url: str
    directory: str
    updated: datetime
    _: KW_ONLY
    filetype: KTJFiletype = None
    group: str = None
    change: bool = False
    path: str = None
    _downloaded: bool = False

    @staticmethod
    def from_existing(filepath: str) -> KTJFile:
        file = KTJFile(basename(filepath), None, dirname(filepath), None)
        file._downloaded = True
        return file

    def __post_init__(self):
        dateformat = '%Y%m%d%H%M'
        base, _ = splitext(self.filename)
        parts = base.split('_')

        self.path = path_join(normpath(self.directory), self.filename)

        if parts[1] == 'ATP':
            # Group |             | Epoch
            # 123456_ATP_123456789_1234567890123.zip
            self.filetype = KTJFiletype.CADASTRAL
            self.group = parts[0]
            # This is not correct, but pretty close. Epoch is in milliseconds.
            updated = datetime.fromtimestamp(int(parts[3]) / 1000, UTC)
        elif parts[-2] == 'muutokset':
            # Group                                                      |
            # Kunta_(omat|toisten)_(kaikki|sijainti|ominaisuus)_muutokset_202401010000.zip
            self.filetype = KTJFiletype.CADASTRAL
            self.group = ''.join(parts[:-1])
            self.change = True
            updated = datetime.strptime(parts[-1], dateformat).replace(tzinfo=UTC)
        elif parts[1] == 'lainhuudot':
            # Group                      |
            # Kunta_lainhuudot_muuttuneet_202401010000.zip
            # Group           |
            # Kunta_lainhuudot_202401010000.zip
            self.filetype = KTJFiletype.CONTACT
            self.group = ''.join(parts[:-1])
            self.change = parts[2] == 'muuttuneet'
            updated = datetime.strptime(parts[-1], dateformat).replace(tzinfo=UTC)
        else:
            raise RuntimeError(f'Unknown file variant: {self.filename}')

        if self.updated is None:
            self.updated = updated

    def download(self, auth: tuple[str, str]):
        if self._downloaded:
            return

        req = requests.get(self.url, auth=auth)

        if req.status_code >= 400:
            raise RuntimeError(f'Failed download: {self.url}, code: {req.status_code}')

        with open(self.path, mode='xb') as handle:
            for data in req.iter_content(chunk_size=2**20):
                handle.write(data)

        self._downloaded = True

    def open(self) -> KTJZipFile:
        if not self._downloaded:
            raise RuntimeError(f'File not downloaded: {self.filename}')

        return KTJZipFile(self)
