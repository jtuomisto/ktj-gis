from __future__ import annotations
from dataclasses import fields as dataclass_fields, asdict
from psycopg import Cursor
from psycopg.sql import SQL, Identifier, Placeholder, Composable, Literal
from ktj_gis.ktjitems import *


class Query:
    def __init__(self, schema: str='public'):
        self._schema = schema
        self._primary = None

    def _table_name(self, item: KTJItem) -> str:
        if isinstance(item, KTJItem):
            return item.__class__.__name__.lower()
        elif issubclass(item, KTJItem):
            return item.__name__.lower()

    def _table_identifier(self, item: KTJItem) -> Composable:
        if isinstance(item, str):
            return Identifier(self._schema, item)

        return Identifier(self._schema, self._table_name(item))

    def _conflict_fields(self) -> Composable:
        if self._primary is None:
            return None

        if isinstance(self._primary, str):
            return Identifier(self._primary)

        return SQL(',').join(map(lambda x: Identifier(x), self._primary))

    def _condition_newer(self, item: KTJItem) -> Composable:
        tbl = self._table_name(item)
        return SQL('{} < {}').format(Identifier(tbl, 'tieto_pvm'), Identifier('excluded', 'tieto_pvm'))

    def _condition_equal(self, field_name: str) -> Composable:
        return SQL('{} = {}').format(Identifier(field_name), Placeholder(field_name))

    def _condition_all(self, *args) -> Composable:
        return SQL(' AND ').join(args)

    def _fields_updated(self, fields: tuple) -> Iterator[Composable]:
        for f in fields:
            yield SQL('{} = {}').format(Identifier(f.name), Identifier('excluded', f.name))

    def _fields(self, item: KTJItem) -> list:
        return [x for x in dataclass_fields(item) if not x.name.startswith('_')]

    def _basic_insert(self, item: KTJItem) -> Composable:
        query = SQL('INSERT INTO {table} ({fields}) VALUES ({values})')
        table = self._table_identifier(item)
        dfields = self._fields(item)
        fields = SQL(',').join(map(lambda x: Identifier(x.name), dfields))
        values = SQL(',').join(map(lambda x: Placeholder(x.name), dfields))
        full_query = query.format(table=table, fields=fields, values=values)

        if (cnflct := self._conflict_fields()) is not None:
            query = SQL('ON CONFLICT ({primary}) DO UPDATE SET {fields} WHERE {condition}')
            fields = SQL(',').join(self._fields_updated(dfields))
            condition = self._condition_newer(item)
            conflict = query.format(primary=cnflct, fields=fields, condition=condition)
            full_query = SQL(' ').join([full_query, conflict])

        return full_query

    def insert(self, cursor: Cursor, item: KTJItem):
        raise NotImplementedError()


class RekisteriyksikkoQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'kiinteistotunnus'
        self._post_insert = self._delete_query()
        self._insert = self._basic_insert(Rekisteriyksikko)

    def _delete_query(self) -> list[Composable]:
        queries = []

        query = SQL('DELETE FROM {table} USING {relation_table} WHERE {conditions}')
        table = self._table_identifier('kiinteistoraja')
        relation_table = self._table_identifier('rekisteriyksikkokiinteistoraja')
        conditions = self._condition_all(
            SQL('fid=kiinteistoraja_fid'),
            SQL('rekisteriyksikkokiinteistoraja.kiinteistotunnus=%(kiinteistotunnus)s')
        )
        queries.append(query.format(table=table, relation_table=relation_table, conditions=conditions))

        table = self._table_identifier('rajamerkki')
        relation_table = self._table_identifier('rekisteriyksikkorajamerkki')
        conditions = self._condition_all(
            SQL('fid=rajamerkki_fid'),
            SQL('rekisteriyksikkorajamerkki.kiinteistotunnus=%(kiinteistotunnus)s')
        )
        queries.append(query.format(table=table, relation_table=relation_table, conditions=conditions))

        query = SQL('DELETE FROM {table} WHERE kiinteistotunnus=%(kiinteistotunnus)s')
        tables = ('muodostuminen', # This deletion cascades to muodostumistoimenpide
                  'palsta',
                  'rekisteriyksikkokiinteistoraja',
                  'rekisteriyksikkorajamerkki',
                  'rekisteriyksikkokaava')

        for tbl in tables:
            queries.append(query.format(table=self._table_identifier(tbl)))

        return queries

    def insert(self, cursor: Cursor, item: Rekisteriyksikko):
        params = asdict(item)
        cursor.execute(self._insert, params)

        if cursor.rowcount == 0:
            return

        item._modified = True

        for q in self._post_insert:
            cursor.execute(q, params)


class RekisteriyksikkoKiinteistorajaQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = ('kiinteistotunnus', 'kiinteistoraja_fid')
        self._insert = self._basic_insert(RekisteriyksikkoKiinteistoraja)

    def insert(self, cursor: Cursor, item: RekisteriyksikkoKiinteistoraja):
        if item._parent._modified:
            cursor.execute(self._insert, asdict(item))


class RekisteriyksikkoRajamerkkiQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = ('kiinteistotunnus', 'rajamerkki_fid')
        self._insert = self._basic_insert(RekisteriyksikkoRajamerkki)

    def insert(self, cursor: Cursor, item: RekisteriyksikkoRajamerkki):
        if item._parent._modified:
            cursor.execute(self._insert, asdict(item))


class RekisteriyksikkoKaavaQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = ('kiinteistotunnus', 'kaavatunnus')
        self._insert = self._basic_insert(RekisteriyksikkoKaava)

    def insert(self, cursor: Cursor, item: RekisteriyksikkoKaava):
        if item._parent._modified:
            cursor.execute(self._insert, asdict(item))


class PalstaQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'fid'
        self._insert = self._basic_insert(Palsta)

    def insert(self, cursor: Cursor, item: Palsta):
        if item._parent._modified:
            cursor.execute(self._insert, asdict(item))


class MuodostuminenQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._insert = self._basic_insert(Muodostuminen)

    def insert(self, cursor: Cursor, item: Muodostuminen):
        if item._parent._modified:
            cursor.execute(self._insert, asdict(item))
            item._modified = True


class MuodostumistoimenpideQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._insert = self._basic_insert(Muodostumistoimenpide)

    def insert(self, cursor: Cursor, item: Muodostumistoimenpide):
        if item._parent._modified:
            cursor.execute(self._insert, asdict(item))


class MaaraAlaQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'tunnus'
        self._post_insert = self._delete_query()
        self._insert = self._basic_insert(MaaraAla)

    def _delete_query(self) -> list[Composable]:
        queries = []
        query = SQL('DELETE FROM {table} WHERE tunnus=%(tunnus)s')
        tables = ('maaraalaosa', 'maaraalarekisteriyksikko')

        for t in tables:
            queries.append(query.format(table=self._table_identifier(t)))

        return queries

    def insert(self, cursor: Cursor, item: MaaraAla):
        params = asdict(item)
        cursor.execute(self._insert, params)

        if cursor.rowcount == 0:
            return

        item._modified = True

        for q in self._post_insert:
            cursor.execute(q, params)


class MaaraAlaRekisteriyksikkoQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._insert = self._basic_insert(MaaraAlaRekisteriyksikko)

    def insert(self, cursor: Cursor, item: MaaraAlaRekisteriyksikko):
        if item._parent._modified:
            cursor.execute(self._insert, asdict(item))


class MaaraAlaOsaQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'fid'
        self._insert = self._basic_insert(MaaraAlaOsa)

    def insert(self, cursor: Cursor, item: MaaraAlaOsa):
        if item._parent._modified:
            cursor.execute(self._insert, asdict(item))


class KayttooikeusyksikkoQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'tunnus'
        self._post_insert = self._delete_query()
        self._insert = self._basic_insert(Kayttooikeusyksikko)

    def _delete_query(self) -> list[Composable]:
        queries = []
        query = SQL('DELETE FROM {table} WHERE tunnus=%(tunnus)s')
        tables = ('kayttooikeusyksikkoosa_piste',
                  'kayttooikeusyksikkoosa_viiva',
                  'kayttooikeusyksikkoosa_alue')

        for t in tables:
            queries.append(query.format(table=self._table_identifier(t)))

        return queries

    def insert(self, cursor: Cursor, item: Kayttooikeusyksikko):
        params = asdict(item)
        cursor.execute(self._insert, params)

        if cursor.rowcount == 0:
            return

        item._modified = True

        for q in self._post_insert:
            cursor.execute(q, params)


class KayttooikeusyksikkoOsaQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'fid'
        self._insert = {}

    def _table_name(self, item: KayttooikeusyksikkoOsa) -> str:
        base = super()._table_name(item)

        match item.geom.GetGeometryType():
            case ogr.wkbPoint:
                return base + '_piste'
            case ogr.wkbLineString:
                return base + '_viiva'
            case ogr.wkbPolygon:
                return base + '_alue'

    def insert(self, cursor: Cursor, item: KayttooikeusyksikkoOsa):
        if not item._parent._modified:
            return

        table = self._table_name(item)

        if table not in self._insert:
            self._insert[table] = self._basic_insert(item)

        cursor.execute(self._insert[table], asdict(item))


class KiinteistorajaQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'fid'
        self._insert = self._basic_insert(Kiinteistoraja)

    def insert(self, cursor: Cursor, item: Kiinteistoraja):
        cursor.execute(self._insert, asdict(item))


class RajamerkkiQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'fid'
        self._insert = self._basic_insert(Rajamerkki)

    def insert(self, cursor: Cursor, item: Rajamerkki):
        cursor.execute(self._insert, asdict(item))


class HenkiloQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = 'tunnus'
        self._insert = self._basic_insert(Henkilo)

    def insert(self, cursor: Cursor, item: Henkilo):
        cursor.execute(self._insert, asdict(item))


class YhteystiedotQuery(Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._primary = ('sijaintitunnus', 'henkilon_tunnus')
        self._pre_insert = self._delete_query()
        self._insert = self._basic_insert(Yhteystieto)

    def _delete_query(self) -> Composable:
        query = SQL('DELETE FROM {table} WHERE sijaintitunnus=%(sijaintitunnus)s')
        return query.format(table=self._table_identifier('yhteystieto'))

    def insert(self, cursor: Cursor, item: Yhteystiedot):
        cursor.execute(self._pre_insert, asdict(item))

        if item._deleted:
            return

        cursor.executemany(self._insert, map(lambda x: asdict(x), item.yhteystiedot))
