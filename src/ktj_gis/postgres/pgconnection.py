from __future__ import annotations
from os.path import join as path_join, dirname, normpath
from osgeo import ogr
from psycopg import connect as pg_connect, adapters, Connection, Cursor, Transaction
from psycopg.sql import SQL, Identifier, Literal
from psycopg.adapt import Dumper


class GeometryDumper(Dumper):
    def dump(self, geom: ogr.Geometry) -> bytes:
        return geom.ExportToIsoWkb().hex().encode('utf8')


adapters.register_dumper(ogr.Geometry, GeometryDumper)


class PGConnection:
    def __init__(self, connection_uri: str, epsg_number: int=None, schema: str=None):
        self._connection_uri = connection_uri
        self._connection = None
        self._in_context = 0
        self._epsg = 3067 if epsg_number is None else epsg_number
        self._schema = 'public' if schema is None else schema
        self._table_creation_params = {
            'schema': Identifier(self._schema),
            'epsg_number': Literal(self._epsg)
        }

    def __enter__(self) -> PGConnection:
        self._in_context += 1
        return self

    def __exit__(self, *args):
        self._in_context -= 1

        if self._in_context == 0 and not self._connection.closed:
            self._connection.close()

    @property
    def connection(self) -> Connection:
        if self._connection is None or self._connection.closed:
            raise RuntimeError('No database connection')

        return self._connection

    def cursor(self) -> Cursor:
        return self.connection.cursor()

    def transaction(self) -> Transaction:
        return self.connection.transaction()

    def connect(self) -> PGConnection:
        if self._connection is None or self._connection.closed:
            self._connection = pg_connect(self._connection_uri, autocommit=True)

            if self._schema is not None and self._schema != 'public':
                s = SQL('SET search_path TO {}, public').format(Identifier(self._schema))
                self._connection.execute(s)

        return self

    def execute_files(self, files: list[str], subdir: str=None):
        parent_dir = normpath(path_join(dirname(__file__), 'sql'))

        if subdir is not None:
            parent_dir = path_join(parent_dir, subdir)

        with self.cursor() as cursor:
            with self.transaction():
                for filename in files:
                    filepath = path_join(parent_dir, filename)

                    with open(filepath, mode='rt', encoding='utf8') as handle:
                        statements = handle.read()

                    cursor.execute(SQL(statements).format(**self._table_creation_params))
