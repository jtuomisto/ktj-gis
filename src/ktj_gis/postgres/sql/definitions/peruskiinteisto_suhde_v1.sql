INSERT INTO {schema}.selitteet VALUES
('peruskiinteisto_suhde', 0, 'Ei arvoa'),
('peruskiinteisto_suhde', 101, 'Peruskiinteistön alapuolinen'),
('peruskiinteisto_suhde', 102, 'Peruskiinteistön yläpuolinen')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;
