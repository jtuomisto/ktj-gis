INSERT INTO {schema}.selitteet VALUES
('olotila', 1, 'Voimassaoleva'),
('olotila', 2, 'Lakannut')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;
