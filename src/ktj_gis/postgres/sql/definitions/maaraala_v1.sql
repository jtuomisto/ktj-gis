INSERT INTO {schema}.selitteet VALUES
('maaraala_lakkaamis_syy', 0, 'Voimassaoleva'),
('maaraala_lakkaamis_syy', 1, 'Kiinteistötoimitus'),
('maaraala_lakkaamis_syy', 2, 'Luovutettu kokonaan osamääräaloiksi'),
('maaraala_lakkaamis_syy', 3, 'Luovutus purettu'),
('maaraala_lakkaamis_syy', 4, 'Virheen korjaus'),
('maaraala_lakkaamis_syy', 5, 'Kiinteistörekisterinpitäjän päätös'),
('maaraala_lakkaamis_syy', 6, 'Kirjaamisviranomaisen ilmoitus (rekisteriyksikön omistajalle siirtynyt määräala)'),
('maaraala_lakkaamis_syy', 9, 'Ei tiedossa')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.selitteet VALUES
('maaraala_tyyppi', 0, 'Tuntematon'),
('maaraala_tyyppi', 1, 'Määräala'),
('maaraala_tyyppi', 2, 'Erillisenä luovutettu yhteisalueosuus')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.selitteet VALUES
('maaraala_saantotapa', 0, 'Tuntematon'),
('maaraala_saantotapa', 1, 'Luovutettu'),
('maaraala_saantotapa', 2, 'Aikaisemman määräalan lisäksi luovutettu'),
('maaraala_saantotapa', 3, 'Pidätetty'),
('maaraala_saantotapa', 4, 'Emäkiinteistön omistajalle erotettava')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.selitteet VALUES
('maaraala_tietolahde', 0, 'Tuntematon'),
('maaraala_tietolahde', 1, 'Kiinteistönluovutusilmoitus'),
('maaraala_tietolahde', 2, 'Käräjäoikeus'),
('maaraala_tietolahde', 3, 'Verotoimisto'),
('maaraala_tietolahde', 4, 'Maanmittauslaitos'),
('maaraala_tietolahde', 5, 'Kunnan kiinteistöinsinööri'),
('maaraala_tietolahde', 6, 'Päätös toiseen kuntaan siirtämisestä'),
('maaraala_tietolahde', 9, 'Ei tiedossa')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.selitteet VALUES
('maaraala_olotila', 0, 'Tuntematon'),
('maaraala_olotila', 1, 'Erottamaton'),
('maaraala_olotila', 2, 'Muodostettu osittain rekisteriyksiköksi'),
('maaraala_olotila', 3, 'Lakannut'),
('maaraala_olotila', 4, 'Virheellisesti rekisteröity')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;
