INSERT INTO {schema}.selitteet VALUES
('rajamerkki_laji', 0, 'Tuntematon'),
('rajamerkki_laji', 600, 'Rajapyykki'),
('rajamerkki_laji', 609, 'Rajapiste'),
('rajamerkki_laji', 610, 'Rajaviitta'),
('rajamerkki_laji', 650, 'Suuntapyykki')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.selitteet VALUES
('rajamerkki_olemassaolo', 0, 'Tuntematon'),
('rajamerkki_olemassaolo', 1, 'Olemassa'),
('rajamerkki_olemassaolo', 2, 'Kadonnut'),
('rajamerkki_olemassaolo', 3, 'Ei rakennettu')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.selitteet VALUES
('rajamerkki_rakenne', 0, 'Ei määritelty'),
('rajamerkki_rakenne', 1, 'Putki'),
('rajamerkki_rakenne', 2, 'Pultti'),
('rajamerkki_rakenne', 3, 'Yksikivinen'),
('rajamerkki_rakenne', 4, 'Nelikulmainen'),
('rajamerkki_rakenne', 5, 'Viisikivinen'),
('rajamerkki_rakenne', 6, 'Yksipaaluinen'),
('rajamerkki_rakenne', 7, 'Viisipaaluinen'),
('rajamerkki_rakenne', 8, 'Reikä, ura, putki tai pultti kiinteässä alustassa'),
('rajamerkki_rakenne', 9, 'Peräkkäiset kivet'),
('rajamerkki_rakenne', 10, 'Peräkkäiset puupaalut'),
('rajamerkki_rakenne', 11, 'Oja')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.selitteet VALUES
('rajamerkki_maanpinta_suhde', 0, 'Tuntematon'),
('rajamerkki_maanpinta_suhde', 1, 'Näkyvissä'),
('rajamerkki_maanpinta_suhde', 2, 'Upotettu')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;
