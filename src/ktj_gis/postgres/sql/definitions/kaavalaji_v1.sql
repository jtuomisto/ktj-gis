INSERT INTO {schema}.selitteet VALUES
('kaavalaji', 2001, 'Yleiskaava'),
('kaavalaji', 2101, 'Asemakaava'),
('kaavalaji', 2102, 'Asemakaava (ohjeellinen tonttijako)'),
('kaavalaji', 2103, 'Ranta-asemakaava'),
('kaavalaji', 2109, 'Asemakaava maanalaisia tiloja varten'),
('kaavalaji', 2107, 'Rakennuskaava (Ahvenanmaa)')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;
