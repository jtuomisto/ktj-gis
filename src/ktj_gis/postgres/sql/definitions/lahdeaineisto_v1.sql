INSERT INTO {schema}.selitteet VALUES
('lahdeaineisto', 0, 'Tuntematon'),
('lahdeaineisto', 4, 'Kuvamittaus'),
('lahdeaineisto', 14, 'Digitointi'),
('lahdeaineisto', 20, 'Maastomittaus'),
('lahdeaineisto', 30, 'Kiinteistötoimitus'),
('lahdeaineisto', 50, 'Kaavalaskenta'),
('lahdeaineisto', 60, 'Kiinteistötoimitus')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;
