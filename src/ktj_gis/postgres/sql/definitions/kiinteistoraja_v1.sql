INSERT INTO {schema}.selitteet VALUES
('kiinteistoraja_laji', 0, 'Tuntematon'),
('kiinteistoraja_laji', 651, 'Valtakunnanraja'),
('kiinteistoraja_laji', 653, 'Kunnanraja'),
('kiinteistoraja_laji', 661, 'Kylän tai kunnanosan raja'),
('kiinteistoraja_laji', 696, 'Rekisteriyksikön raja'),
('kiinteistoraja_laji', 697, 'Tekninen apuviiva')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;
