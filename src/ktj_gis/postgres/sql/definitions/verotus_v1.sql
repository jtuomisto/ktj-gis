INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_rak_tyyppi',  '110', 'Omakotitalot'),
('rakennus_rak_tyyppi',  '111', 'Paritalot'),
('rakennus_rak_tyyppi',  '112', 'Rivitalot'),
('rakennus_rak_tyyppi',  '120', 'Pienkerrostalot'),
('rakennus_rak_tyyppi',  '121', 'Asuinkerrostalot'),
('rakennus_rak_tyyppi',  '130', 'Asuntolarakennukset'),
('rakennus_rak_tyyppi',  '140', 'Erityisryhmien asuinrakennukset'),
('rakennus_rak_tyyppi',  '210', 'Ympärivuotiseen käyttöön soveltuvat vapaa-ajan asuinrakennukset'),
('rakennus_rak_tyyppi',  '211', 'Osavuotiseen käyttöön soveltuvat vapaa-ajan asuinrakennukset'),
('rakennus_rak_tyyppi',  '310', 'Tukku- ja vähittäiskaupan myymälähallit'),
('rakennus_rak_tyyppi',  '311', 'Kauppakeskukset ja liike- ja tavaratalot'),
('rakennus_rak_tyyppi',  '319', 'Muut myymälärakennukset'),
('rakennus_rak_tyyppi',  '320', 'Hotellit'),
('rakennus_rak_tyyppi',  '321', 'Motellit, hostellit ja vastaavat majoitusliikerakennukset'),
('rakennus_rak_tyyppi',  '322', 'Loma-, lepo- ja virkistyskodit'),
('rakennus_rak_tyyppi',  '329', 'Muut majoitusliikerakennukset'),
('rakennus_rak_tyyppi',  '330', 'Ravintolarakennukset ja vastaavat liikerakennukset'),
('rakennus_rak_tyyppi',  '400', 'Toimistorakennukset'),
('rakennus_rak_tyyppi',  '510', 'Asemarakennukset ja terminaalit'),
('rakennus_rak_tyyppi',  '511', 'Ammattiliikenteen kaluston suojarakennukset'),
('rakennus_rak_tyyppi',  '512', 'Ammattiliikenteen kaluston huoltorakennukset'),
('rakennus_rak_tyyppi',  '513', 'Pysäköintitalot ja -hallit'),
('rakennus_rak_tyyppi',  '514', 'Kulkuneuvojen katokset'),
('rakennus_rak_tyyppi',  '520', 'Datakeskukset ja laitetilat'),
('rakennus_rak_tyyppi',  '521', 'Tietoliikenteen rakennukset'),
('rakennus_rak_tyyppi',  '590', 'Muut liikenteen rakennukset'),
('rakennus_rak_tyyppi',  '610', 'Terveys- ja hyvinvointikeskukset'),
('rakennus_rak_tyyppi',  '611', 'Keskussairaalat'),
('rakennus_rak_tyyppi',  '612', 'Erikoissairaalat ja laboratoriorakennukset'),
('rakennus_rak_tyyppi',  '613', 'Muut sairaalat'),
('rakennus_rak_tyyppi',  '614', 'Kuntoutuslaitokset'),
('rakennus_rak_tyyppi',  '619', 'Muut terveydenhuoltorakennukset'),
('rakennus_rak_tyyppi',  '620', 'Laitospalvelujen rakennukset'),
('rakennus_rak_tyyppi',  '621', 'Avopalvelujen rakennukset'),
('rakennus_rak_tyyppi',  '630', 'Vankilarakennukset'),
('rakennus_rak_tyyppi',  '710', 'Teatterit, musiikki- ja kongressitalot'),
('rakennus_rak_tyyppi',  '711', 'Elokuvateatterit'),
('rakennus_rak_tyyppi',  '712', 'Kirjastot ja arkistot'),
('rakennus_rak_tyyppi',  '713', 'Museot ja taidegalleriat'),
('rakennus_rak_tyyppi',  '714', 'Näyttely- ja messuhallit'),
('rakennus_rak_tyyppi',  '720', 'Seura- ja kerhorakennukset'),
('rakennus_rak_tyyppi',  '730', 'Uskonnonharjoittamisrakennukset'),
('rakennus_rak_tyyppi',  '731', 'Seurakuntatalot'),
('rakennus_rak_tyyppi',  '739', 'Muut uskonnollisten yhteisöjen rakennukset'),
('rakennus_rak_tyyppi',  '740', 'Jäähallit'),
('rakennus_rak_tyyppi',  '741', 'Uimahallit'),
('rakennus_rak_tyyppi',  '742', 'Monitoimihallit'),
('rakennus_rak_tyyppi',  '743', 'Urheilu- ja palloiluhallit'),
('rakennus_rak_tyyppi',  '744', 'Stadion- ja katsomorakennukset'),
('rakennus_rak_tyyppi',  '749', 'Muut urheilu- ja liikuntarakennukset'),
('rakennus_rak_tyyppi',  '790', 'Muut kokoontumisrakennukset'),
('rakennus_rak_tyyppi',  '810', 'Lasten päiväkodit'),
('rakennus_rak_tyyppi',  '820', 'Yleissivistävien oppilaitosten rakennukset'),
('rakennus_rak_tyyppi',  '830', 'Ammatillisten oppilaitosten rakennukset'),
('rakennus_rak_tyyppi',  '840', 'Korkeakoulurakennukset'),
('rakennus_rak_tyyppi',  '841', 'Tutkimuslaitosrakennukset'),
('rakennus_rak_tyyppi',  '890', 'Vapaan sivistystyön opetusrakennukset'),
('rakennus_rak_tyyppi',  '891', 'Järjestöjen, liittojen, työnantajien ja vastaavien opetusrakennukset'),
('rakennus_rak_tyyppi',  '910', 'Yleiskäyttöiset teollisuushallit'),
('rakennus_rak_tyyppi',  '911', 'Raskaan teollisuuden tehdasrakennukset'),
('rakennus_rak_tyyppi',  '912', 'Elintarviketeollisuuden tuotantorakennukset'),
('rakennus_rak_tyyppi',  '919', 'Muut teollisuuden tuotantorakennukset'),
('rakennus_rak_tyyppi',  '920', 'Teollisuus- ja pienteollisuustalot'),
('rakennus_rak_tyyppi',  '930', 'Metallimalmien käsittelyrakennukset'),
('rakennus_rak_tyyppi',  '939', 'Muut kaivannaistoiminnan rakennukset'),
('rakennus_rak_tyyppi', '1010', 'Sähköenergian tuotantorakennukset'),
('rakennus_rak_tyyppi', '1011', 'Lämpö- ja kylmäenergian tuotantorakennukset'),
('rakennus_rak_tyyppi', '1090', 'Energiansiirtorakennukset'),
('rakennus_rak_tyyppi', '1091', 'Energianvarastointirakennukset'),
('rakennus_rak_tyyppi', '1110', 'Vedenotto-, vedenpuhdistus- ja vedenjakelurakennukset'),
('rakennus_rak_tyyppi', '1120', 'Jätteenkeruu-, jätteenkäsittely- ja jätteenvarastointirakennukset'),
('rakennus_rak_tyyppi', '1130', 'Materiaalien kierrätysrakennukset'),
('rakennus_rak_tyyppi', '1210', 'Lämmittämättömät varastot'),
('rakennus_rak_tyyppi', '1211', 'Lämpimät varastot'),
('rakennus_rak_tyyppi', '1212', 'Kylmä- ja pakastevarastot'),
('rakennus_rak_tyyppi', '1213', 'Muut olosuhteiltaan säädellyt varastot'),
('rakennus_rak_tyyppi', '1214', 'Logistiikkakeskukset ja muut monikäyttöiset varastorakennukset'),
('rakennus_rak_tyyppi', '1215', 'Varastokatokset'),
('rakennus_rak_tyyppi', '1310', 'Paloasemat'),
('rakennus_rak_tyyppi', '1311', 'Väestönsuojat'),
('rakennus_rak_tyyppi', '1319', 'Muut pelastustoimen rakennukset'),
('rakennus_rak_tyyppi', '1410', 'Lypsykarjarakennukset'),
('rakennus_rak_tyyppi', '1411', 'Lihakarjarakennukset'),
('rakennus_rak_tyyppi', '1412', 'Sikalat'),
('rakennus_rak_tyyppi', '1413', 'Lampolat ja vuohinavetat'),
('rakennus_rak_tyyppi', '1414', 'Hevostallit'),
('rakennus_rak_tyyppi', '1415', 'Siipikarjarakennukset'),
('rakennus_rak_tyyppi', '1416', 'Turkiseläinrakennukset'),
('rakennus_rak_tyyppi', '1419', 'Muut eläinsuojat'),
('rakennus_rak_tyyppi', '1490', 'Kasvihuoneet'),
('rakennus_rak_tyyppi', '1491', 'Viljankuivaamot ja viljansäilytysrakennukset'),
('rakennus_rak_tyyppi', '1492', 'Maatalouden varastorakennukset'),
('rakennus_rak_tyyppi', '1493', 'Lantalat'),
('rakennus_rak_tyyppi', '1499', 'Muut maa-, metsä- ja kalatalouden rakennukset'),
('rakennus_rak_tyyppi', '1910', 'Saunarakennukset'),
('rakennus_rak_tyyppi', '1911', 'Talousrakennukset'),
('rakennus_rak_tyyppi', '1912', 'Majat ja tuvat'),
('rakennus_rak_tyyppi', '1919', 'Muualla luokittelemattomat rakennukset')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_kayttotarkoitus', 'GENERAL', 'Yleinen'),
('rakennus_kayttotarkoitus', 'NPOWER', 'Ydinvoimala'),
('rakennus_kayttotarkoitus', 'OTHERRES', 'Muu kuin vakituinen asuinkäyttö'),
('rakennus_kayttotarkoitus', 'PERMRES', 'Vakituinen asuinkäyttö'),
('rakennus_kayttotarkoitus', 'POWER', 'Voimalaitos'),
('rakennus_kayttotarkoitus', 'SPOWER', 'Pienvoimalaitos')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_osa_rak_tyyppi', 'AIR DOME', 'Ylipainehalli'),
('rakennus_osa_rak_tyyppi', 'BARRACKS', 'Kasarmirakennus'),
('rakennus_osa_rak_tyyppi', 'BOMB', 'Kalliosuoja (väestönsuoja)'),
('rakennus_osa_rak_tyyppi', 'CANTEEN', 'Ruokala'),
('rakennus_osa_rak_tyyppi', 'CAR PARK', 'Pysäköintitalo'),
('rakennus_osa_rak_tyyppi', 'CENTRAL HOSP', 'Keskussairaala'),
('rakennus_osa_rak_tyyppi', 'CHURCH', 'Kirkko'),
('rakennus_osa_rak_tyyppi', 'COMM', 'Tietoliikennerakennus'),
('rakennus_osa_rak_tyyppi', 'COTTAGE', 'Siirtolapuutarha'),
('rakennus_osa_rak_tyyppi', 'COURSE', 'Kurssikeskus'),
('rakennus_osa_rak_tyyppi', 'DIST HOSP', 'Aluesairaala'),
('rakennus_osa_rak_tyyppi', 'ENERGY', 'Tuuli- tai aurinkovoimala'),
('rakennus_osa_rak_tyyppi', 'FIRE', 'Paloasema'),
('rakennus_osa_rak_tyyppi', 'GREENHOUSE', 'Kasvihuone'),
('rakennus_osa_rak_tyyppi', 'HEALTH', 'Terveyskeskus'),
('rakennus_osa_rak_tyyppi', 'HOLIDAY', 'Vapaa-ajan asunto'),
('rakennus_osa_rak_tyyppi', 'HOTEL', 'Hotelli'),
('rakennus_osa_rak_tyyppi', 'HYDROFORCE', 'Vesivoimalaitos'),
('rakennus_osa_rak_tyyppi', 'ICE', 'Jäähalli'),
('rakennus_osa_rak_tyyppi', 'INDUSTRIAL', 'Teollisuus- ja varastorakennus'),
('rakennus_osa_rak_tyyppi', 'LIBRARY', 'Kirjasto- tai arkistorakennus'),
('rakennus_osa_rak_tyyppi', 'MUSEUM', 'Museo tai taidegalleria'),
('rakennus_osa_rak_tyyppi', 'NUCLEAR', 'Ydinvoimala'),
('rakennus_osa_rak_tyyppi', 'NURSE', 'Hoitolaitos'),
('rakennus_osa_rak_tyyppi', 'OFFICE', 'Toimistorakennus'),
('rakennus_osa_rak_tyyppi', 'ONEFAM', 'Pientalo'),
('rakennus_osa_rak_tyyppi', 'OTHER', 'Muu rakennus'),
('rakennus_osa_rak_tyyppi', 'OUTBUILD', 'Talousrakennus'),
('rakennus_osa_rak_tyyppi', 'PARISH', 'Seurakuntatalo'),
('rakennus_osa_rak_tyyppi', 'POWER PLANT', 'Voimalaitos'),
('rakennus_osa_rak_tyyppi', 'PRISON', 'Vankila'),
('rakennus_osa_rak_tyyppi', 'RES BLOCK', 'Asuinkerrostalo'),
('rakennus_osa_rak_tyyppi', 'SAUNA', 'Sauna'),
('rakennus_osa_rak_tyyppi', 'SEC SCHOOL', 'Peruskoulu- tai lukiorakennus'),
('rakennus_osa_rak_tyyppi', 'SHELTER', 'Kalliosuoja (varasto)'),
('rakennus_osa_rak_tyyppi', 'SHOP', 'Myymälärakennus'),
('rakennus_osa_rak_tyyppi', 'SILO', 'Siilo'),
('rakennus_osa_rak_tyyppi', 'SPORTS', 'Urheilutalo'),
('rakennus_osa_rak_tyyppi', 'STADIUM', 'Stadion- tai katsomorakennus'),
('rakennus_osa_rak_tyyppi', 'SWIM POOL', 'Uimahalli'),
('rakennus_osa_rak_tyyppi', 'TERMINAL', 'Asemarakennus tai terminaali'),
('rakennus_osa_rak_tyyppi', 'THEATRE', 'Teatterirakennus'),
('rakennus_osa_rak_tyyppi', 'UNIVERSITY', 'Korkeakoulu'),
('rakennus_osa_rak_tyyppi', 'WATER', 'Vesitorni'),
('rakennus_osa_rak_tyyppi', 'VOCATION SCH', 'Ammattikoulu'),
('rakennus_osa_rak_tyyppi', 'YOUTH', 'Nuorisotalo')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_osa_kantava_rak', 'WOOD', 'Puu'),
('rakennus_osa_kantava_rak', 'STONE', 'Kivi tai metalli')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_osa_lammitys', 'CENTRAL', 'Keskuslämmitys'),
('rakennus_osa_lammitys', 'NOCENTRAL', 'Muu kuin keskuslämmitys')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_osa_muoto', 'H', 'H/T/U-muotoinen'),
('rakennus_osa_muoto', 'L', 'L-muotoinen'),
('rakennus_osa_muoto', 'RECT', 'Suorakaide')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_osa_ilmastointi', '1', 'Ei koneellista ilmastointia'),
('rakennus_osa_ilmastointi', '2', 'Koneellinen sisään ja ulospuhallus'),
('rakennus_osa_ilmastointi', '3', 'Koneellinen ilmastointi, jäähdytysjärjelmä')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_osa_lammitys_vesi', '1', 'Pääasiassa lämmittämätön varastorakennus, jossa ei ole sosiaali- tai toimistotiloja '),
('rakennus_osa_lammitys_vesi', '2', 'Pääasiassa lämmittämätön rakennus, jossa on vain vähän vesipisteitä ja sosiaali- ja toimistotiloja enintään 3 % pinta-alasta. '),
('rakennus_osa_lammitys_vesi', '3', 'Pääasiassa hallimaista teollisuustilaa, jota ei lämmitetä yli 18 °C:seen. Sosiaali- ja toimistotiloja on yli 3 % mutta alle 15 % rakennuksen pinta-alasta.'),
('rakennus_osa_lammitys_vesi', '4', 'Tuotantotilojen lämpötila on yleensä yli 18 °C. Sosiaali- ja toimistotiloja on vähintään 15 % rakennuksen pinta-alasta tai ns. märkiä tiloja yli 30 % rakennuksen pinta-alasta. Automaattinen palosammutusjärjestelmä.')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_osa_siilo_tyyppi', 'RECTFUNNEL', 'Suorakulmainen pohja'),
('rakennus_osa_siilo_tyyppi', 'ROUNDFLAT', 'Pyöreä, betoninen, tasapohjainen'),
('rakennus_osa_siilo_tyyppi', 'ROUNDFUNNEL','Pyöreä, betoninen, suppilopohjainen'),
('rakennus_osa_siilo_tyyppi', 'ROUNDSTEEL', 'Pyöreä, teräsrakenteinen')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('rakennus_osa_kasvihuone', 'ACRYLIC', 'Akryylikate, teräsrunko, lämmityslaitteet'),
('rakennus_osa_kasvihuone', 'GLASSHEAT', 'Lasikate, teräsrunko, lämmitys- ja kastelulaitteet'),
('rakennus_osa_kasvihuone', 'GLASSNOHEAT', 'Lasikate, teräsrunko, ei lämmitys- ja kastelulaitteita'),
('rakennus_osa_kasvihuone', 'WOODHEAT', 'Muovikate, puurunko, lämmitys- ja kastelulaitteet'),
('rakennus_osa_kasvihuone', 'WOODNOHEAT', 'Muovikate, puurunko, ei lämmitys- ja kastelulaitteita')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('maapohja_tyyppi', '3D', '3D-maapohja'),
('maapohja_tyyppi', 'AG PROD', 'Maatalouden tuotantorakennuksen rakennuspaikka'),
('maapohja_tyyppi', 'BUILDING', 'Rakennusmaa'),
('maapohja_tyyppi', 'FOR PROD', 'Metsätalouden tuotantorakennuksen rakennuspaikka'),
('maapohja_tyyppi', 'OTHER', 'Muu maa'),
('maapohja_tyyppi', 'SOIL', 'Maa-aineksen ottoalue')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('maapohja_kayttotarkoitus', 'GENERAL', 'Yleinen käyttötarkoitus'),
('maapohja_kayttotarkoitus', 'PUBLIC', 'Yleishyödyllisessä käytössä'),
('maapohja_kayttotarkoitus', 'VACANT', 'Rakentamaton rakennuspaikka')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('maapohja_kaavalaji', 'NONE', 'Ei asemakaavaa'),
('maapohja_kaavalaji', 'SHORE', 'Ranta-asemakaava'),
('maapohja_kaavalaji', 'TOWN', 'Asemakaava')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('maapohja_ranta', 'NO SHORE', 'Ei rajoitu rantaan tai vesijättöön'),
('maapohja_ranta', 'SHORE', 'Kiinteistö rajoittuu rantaan tai vesijättöön'),
('maapohja_ranta', 'RIPARIAN', 'Rantaoikeus')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('maapohja_laskentaperuste', '1', 'Pinta-ala'),
('maapohja_laskentaperuste', '2', 'Rakennusoikeus')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;

INSERT INTO {schema}.vero_selitteet VALUES
('muu_profiili_tyyppi', 'AGBUILDING', 'Maatalouden tuotantorakennukset'),
('muu_profiili_tyyppi', 'FORBUILDING', 'Metsätalouden tuotantorakennukset'),
('muu_profiili_tyyppi', 'HYDRO', 'Vesivoima')
ON CONFLICT (kohde, tunniste) DO UPDATE SET selite = EXCLUDED.selite;
