CREATE TABLE IF NOT EXISTS {schema}.rekisteriyksikkokiinteistoraja(
    kiinteistotunnus text NOT NULL REFERENCES rekisteriyksikko (kiinteistotunnus) ON DELETE CASCADE,
    kiinteistoraja_fid bigint NOT NULL REFERENCES kiinteistoraja (fid) ON DELETE CASCADE,
    tieto_pvm timestamptz NOT NULL,
    PRIMARY KEY (kiinteistotunnus, kiinteistoraja_fid)
);
