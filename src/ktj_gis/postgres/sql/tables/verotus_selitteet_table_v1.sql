CREATE TABLE IF NOT EXISTS {schema}.vero_selitteet(
    kohde text NOT NULL,
    tunniste text NOT NULL,
    selite text NOT NULL,
    PRIMARY KEY (kohde, tunniste)
);
