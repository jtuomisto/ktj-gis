CREATE TABLE IF NOT EXISTS {schema}.muodostuminen(
    tunnus text NOT NULL PRIMARY KEY,
    kiinteistotunnus text NOT NULL REFERENCES rekisteriyksikko (kiinteistotunnus) ON DELETE CASCADE,
    laji integer NOT NULL,
    toimitus_pvm date,
    muutos_pinta_ala_maa numeric(12, 5) NOT NULL DEFAULT 0,
    muutos_pinta_ala_vesi numeric(12, 5) NOT NULL DEFAULT 0,
    selitys text,
    tieto_pvm timestamptz NOT NULL
);

CREATE INDEX IF NOT EXISTS muodostuminen_kitu_idx ON {schema}.muodostuminen (kiinteistotunnus);
