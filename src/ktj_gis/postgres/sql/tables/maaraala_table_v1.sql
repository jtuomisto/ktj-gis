CREATE TABLE IF NOT EXISTS {schema}.maaraala(
    tunnus text PRIMARY KEY,
    olotila integer NOT NULL,
    maara_alan_olotila integer NOT NULL,
    rekisterointi_pvm date,
    lakkaamis_pvm date,
    lakkaamis_syy integer NOT NULL,
    tyyppi integer NOT NULL,
    peruskiinteisto_suhde integer NOT NULL,
    saantotapa integer NOT NULL,
    tietolahde integer NOT NULL,
    alkuperainen_saanto_pvm date,
    tieto_pvm timestamptz NOT NULL
);

CREATE INDEX IF NOT EXISTS maaraala_olotila_idx ON {schema}.maaraala (olotila);
CREATE INDEX IF NOT EXISTS maaraala_m_olotila_idx ON {schema}.maaraala (maara_alan_olotila);
