CREATE TABLE IF NOT EXISTS {schema}.palsta(
    fid bigint PRIMARY KEY,
    kiinteistotunnus text NOT NULL REFERENCES rekisteriyksikko (kiinteistotunnus) ON DELETE CASCADE,
    tieto_pvm timestamptz NOT NULL,
    geom geometry(Polygon, {epsg_number}) NOT NULL
);

CREATE INDEX IF NOT EXISTS palsta_kitu_idx ON {schema}.palsta (kiinteistotunnus);
CREATE INDEX IF NOT EXISTS palsta_geom_idx ON {schema}.palsta USING gist (geom);
