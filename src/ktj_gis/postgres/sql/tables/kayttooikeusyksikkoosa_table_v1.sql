CREATE TABLE IF NOT EXISTS {schema}.kayttooikeusyksikkoosa_piste(
    fid bigint PRIMARY KEY,
    tunnus text NOT NULL REFERENCES kayttooikeusyksikko (tunnus) ON DELETE CASCADE,
    numero integer NOT NULL,
    olotila integer NOT NULL,
    rekisterointi_pvm date,
    tieto_pvm timestamptz NOT NULL,
    geom geometry(Point, {epsg_number}) NOT NULL
);

CREATE TABLE IF NOT EXISTS {schema}.kayttooikeusyksikkoosa_viiva(
    fid bigint PRIMARY KEY,
    tunnus text NOT NULL REFERENCES kayttooikeusyksikko (tunnus) ON DELETE CASCADE,
    numero integer NOT NULL,
    olotila integer NOT NULL,
    rekisterointi_pvm date,
    tieto_pvm timestamptz NOT NULL,
    geom geometry(LineString, {epsg_number}) NOT NULL
);

CREATE TABLE IF NOT EXISTS {schema}.kayttooikeusyksikkoosa_alue(
    fid bigint PRIMARY KEY,
    tunnus text NOT NULL REFERENCES kayttooikeusyksikko (tunnus) ON DELETE CASCADE,
    numero integer NOT NULL,
    olotila integer NOT NULL,
    rekisterointi_pvm date,
    tieto_pvm timestamptz NOT NULL,
    geom geometry(Polygon, {epsg_number}) NOT NULL
);

CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_piste_olotila_idx ON {schema}.kayttooikeusyksikkoosa_piste (olotila);
CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_viiva_olotila_idx ON {schema}.kayttooikeusyksikkoosa_viiva (olotila);
CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_alue_olotila_idx ON {schema}.kayttooikeusyksikkoosa_alue (olotila);

CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_piste_tunnus_idx ON {schema}.kayttooikeusyksikkoosa_piste (tunnus);
CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_viiva_tunnus_idx ON {schema}.kayttooikeusyksikkoosa_viiva (tunnus);
CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_alue_tunnus_idx ON {schema}.kayttooikeusyksikkoosa_alue (tunnus);

CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_piste_geom_idx ON {schema}.kayttooikeusyksikkoosa_piste USING gist (geom);
CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_viiva_geom_idx ON {schema}.kayttooikeusyksikkoosa_viiva USING gist (geom);
CREATE INDEX IF NOT EXISTS kayttooikeusyksikkoosa_alue_geom_idx ON {schema}.kayttooikeusyksikkoosa_alue USING gist (geom);
