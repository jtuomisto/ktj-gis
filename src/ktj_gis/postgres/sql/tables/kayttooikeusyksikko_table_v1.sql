CREATE TABLE IF NOT EXISTS {schema}.kayttooikeusyksikko(
    tunnus text PRIMARY KEY,
    olotila integer NOT NULL,
    laji integer NOT NULL,
    rekisterointi_pvm date,
    nimi text,
    tieto_pvm timestamptz NOT NULL
);

CREATE INDEX IF NOT EXISTS kayttooikeusyksikko_olotila_idx ON {schema}.kayttooikeusyksikko (olotila);
