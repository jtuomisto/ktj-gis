CREATE TABLE IF NOT EXISTS {schema}.maaraalaosa(
    fid bigint PRIMARY KEY,
    tunnus text NOT NULL REFERENCES maaraala (tunnus) ON DELETE CASCADE,
    tieto_pvm timestamptz NOT NULL,
    geom geometry(Point, {epsg_number}) NOT NULL
);

CREATE INDEX IF NOT EXISTS maaraalaosa_tunnus_idx ON {schema}.maaraalaosa (tunnus);
CREATE INDEX IF NOT EXISTS maaraalaosa_geom_idx ON {schema}.maaraalaosa USING gist (geom);
