CREATE TABLE IF NOT EXISTS {schema}.selitteet(
    kohde text NOT NULL,
    tunniste int NOT NULL,
    selite text NOT NULL,
    PRIMARY KEY (kohde, tunniste)
);
