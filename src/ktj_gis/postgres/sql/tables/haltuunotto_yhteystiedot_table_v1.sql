CREATE TABLE IF NOT EXISTS {schema}.haltuunotto_yhteystiedot(
    kiinteistotunnus text NOT NULL REFERENCES haltuunotto (kiinteistotunnus) ON DELETE CASCADE,
    muodostaja_sijaintitunnus text NOT NULL,
    yhteystiedot text NOT NULL,
    tieto_pvm timestamptz NOT NULL DEFAULT current_timestamp,
    PRIMARY KEY (kiinteistotunnus, muodostaja_sijaintitunnus)
);

CREATE INDEX IF NOT EXISTS haltuunotto_yhteystiedot_kitu_idx ON {schema}.haltuunotto_yhteystiedot (kiinteistotunnus);
CREATE INDEX IF NOT EXISTS haltuunotto_yhteystiedot_muod_idx ON {schema}.haltuunotto_yhteystiedot (muodostaja_sijaintitunnus);
