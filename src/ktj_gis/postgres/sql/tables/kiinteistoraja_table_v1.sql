CREATE TABLE IF NOT EXISTS {schema}.kiinteistoraja(
    fid bigint PRIMARY KEY,
    laji integer NOT NULL,
    lahdeaineisto integer NOT NULL,
    luonne1 integer NOT NULL,
    luonne2 integer NOT NULL,
    rekisterointi_pvm date,
    tieto_pvm timestamptz NOT NULL,
    geom geometry(CompoundCurve, {epsg_number}) NOT NULL
);

CREATE INDEX IF NOT EXISTS kiinteistoraja_geom_idx ON {schema}.kiinteistoraja USING gist (geom);
CREATE INDEX IF NOT EXISTS kiinteistoraja_laji_idx ON {schema}.kiinteistoraja (laji);
