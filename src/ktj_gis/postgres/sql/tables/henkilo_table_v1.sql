CREATE TABLE IF NOT EXISTS {schema}.henkilo(
    tunnus text PRIMARY KEY,
    nimi text NOT NULL,
    osoite text,
    y_tunnus text,
    ulkomaalainen boolean,
    tieto_pvm timestamptz NOT NULL
);
