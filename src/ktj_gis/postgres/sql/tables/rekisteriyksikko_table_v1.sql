CREATE TABLE IF NOT EXISTS {schema}.rekisteriyksikko(
    kiinteistotunnus text PRIMARY KEY,
    olotila integer NOT NULL,
    laji integer NOT NULL,
    peruskiinteisto_suhde integer NOT NULL,
    rekisterointi_pvm date,
    lakkaamis_pvm date,
    nimi text,
    pinta_ala_maa numeric(12, 5) NOT NULL DEFAULT 0,
    pinta_ala_vesi numeric(12, 5) NOT NULL DEFAULT 0,
    kayttotarkoitus text,
    osaluku text,
    manttaali text,
    arkistoviite text,
    kuntatunnus integer NOT NULL,
    tieto_pvm timestamptz NOT NULL
);

CREATE INDEX IF NOT EXISTS rekisteriyksikko_olotila_idx ON {schema}.rekisteriyksikko (olotila);
