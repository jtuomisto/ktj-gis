CREATE TABLE IF NOT EXISTS {schema}.haltuunotto_korvaus(
    kiinteistotunnus text NOT NULL REFERENCES haltuunotto (kiinteistotunnus) ON DELETE RESTRICT,
    muodostaja_sijaintitunnus text NOT NULL,
    korvaus numeric(12, 2) NOT NULL,
    tieto_pvm timestamptz NOT NULL DEFAULT current_timestamp,
    PRIMARY KEY (kiinteistotunnus, muodostaja_sijaintitunnus)
);

CREATE INDEX IF NOT EXISTS haltuunotto_korvaus_kitu_idx ON {schema}.haltuunotto_korvaus (kiinteistotunnus);
CREATE INDEX IF NOT EXISTS haltuunotto_korvaus_muod_idx ON {schema}.haltuunotto_korvaus (muodostaja_sijaintitunnus);
