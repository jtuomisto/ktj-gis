CREATE TABLE IF NOT EXISTS {schema}.rekisteriyksikkorajamerkki(
    kiinteistotunnus text NOT NULL REFERENCES rekisteriyksikko (kiinteistotunnus) ON DELETE CASCADE,
    rajamerkki_fid bigint NOT NULL REFERENCES rajamerkki (fid) ON DELETE CASCADE,
    tieto_pvm timestamptz NOT NULL,
    PRIMARY KEY (kiinteistotunnus, rajamerkki_fid)
);
