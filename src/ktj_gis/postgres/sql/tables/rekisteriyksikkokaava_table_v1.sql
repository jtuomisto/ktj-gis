CREATE TABLE IF NOT EXISTS {schema}.rekisteriyksikkokaava(
    kiinteistotunnus text NOT NULL REFERENCES rekisteriyksikko (kiinteistotunnus) ON DELETE CASCADE,
    kaavatunnus text NOT NULL,
    kaavalaji integer NOT NULL,
    hyvaksytty_pvm date,
    voimaantulo_pvm date,
    lakkaamis_pvm date,
    olotila integer NOT NULL,
    arkistotunnus text,
    yksilointitunnus text,
    selitys text,
    tieto_pvm timestamptz NOT NULL,
    PRIMARY KEY (kiinteistotunnus, kaavatunnus)
);

CREATE INDEX IF NOT EXISTS rekisteriyksikkokaava_laji_idx ON {schema}.rekisteriyksikkokaava (kaavalaji);
