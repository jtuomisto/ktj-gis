CREATE TABLE IF NOT EXISTS {schema}.yhteystieto(
    sijaintitunnus text NOT NULL,
    henkilon_tunnus text NOT NULL REFERENCES henkilo (tunnus) ON DELETE CASCADE,
    tieto_pvm timestamptz NOT NULL,
    PRIMARY KEY (sijaintitunnus, henkilon_tunnus)
);

CREATE INDEX IF NOT EXISTS yhteystieto_henkilo_idx ON {schema}.yhteystieto (henkilon_tunnus);
