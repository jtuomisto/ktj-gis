CREATE TABLE IF NOT EXISTS {schema}.haltuunotto(
    kiinteistotunnus text PRIMARY KEY,
    nimi text,
    muodostajat text NOT NULL,
    dokumentti text,
    haltuunotto_pvm date,
    rekisterointi_pvm date,
    tieto_pvm timestamptz NOT NULL,
    geom geometry(MultiPolygon, {epsg_number}) NOT NULL
);

CREATE INDEX IF NOT EXISTS haltuunotto_geom_idx ON {schema}.haltuunotto USING gist (geom);
CREATE INDEX IF NOT EXISTS haltuunotto_pvm_idx ON {schema}.haltuunotto (haltuunotto_pvm);
