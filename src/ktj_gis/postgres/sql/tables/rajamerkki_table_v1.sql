CREATE TABLE IF NOT EXISTS {schema}.rajamerkki(
    fid bigint PRIMARY KEY,
    laji integer NOT NULL,
    numero text,
    sijaintitarkkuus numeric(8, 2),
    rakenne integer NOT NULL,
    lahdeaineisto integer NOT NULL,
    maanpinta_suhde integer NOT NULL,
    olemassaolo integer NOT NULL,
    rekisterointi_pvm date,
    tieto_pvm timestamptz NOT NULL,
    geom geometry(Point, {epsg_number}) NOT NULL
);

CREATE INDEX IF NOT EXISTS rajamerkki_geom_idx ON {schema}.rajamerkki USING gist (geom);
