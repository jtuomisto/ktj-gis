CREATE OR REPLACE VIEW {schema}.rekisteriyksikot AS
SELECT
r.kiinteistotunnus,
r.laji,
s.selite as laji_selite,
r.nimi,
r.rekisterointi_pvm,
r.pinta_ala_maa,
r.pinta_ala_vesi,
r.kayttotarkoitus,
r.peruskiinteisto_suhde,
r.osaluku,
r.manttaali,
r.arkistoviite,
r.tieto_pvm,
p.geom
FROM {schema}.rekisteriyksikko r
LEFT JOIN (
    SELECT kiinteistotunnus, ST_Collect(geom) as geom
    FROM {schema}.palsta
    GROUP BY kiinteistotunnus
) p USING (kiinteistotunnus)
LEFT JOIN {schema}.selitteet s ON s.kohde = 'rekisteriyksikko_laji' AND s.tunniste = r.laji
WHERE r.olotila != 2;
