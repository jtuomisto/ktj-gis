CREATE OR REPLACE VIEW {schema}.maaraalaosat AS
SELECT
m.fid,
m.tunnus,
ma.maara_alan_olotila as m_olotila,
smo.selite as m_olotila_selite,
ma.rekisterointi_pvm,
ma.tyyppi,
sty.selite as tyyppi_selite,
ma.peruskiinteisto_suhde,
ma.saantotapa,
sms.selite as saantotapa_selite,
ma.alkuperainen_saanto_pvm,
ma.lakkaamis_syy,
sml.selite as lakkaamis_syy_selite,
ma.lakkaamis_pvm,
ma.tietolahde,
smt.selite as tietolahde_selite,
m.tieto_pvm,
m.geom
FROM {schema}.maaraalaosa m
LEFT JOIN {schema}.maaraala ma USING (tunnus)
LEFT JOIN {schema}.selitteet smo ON smo.kohde = 'maaraala_olotila' AND smo.tunniste = ma.maara_alan_olotila
LEFT JOIN {schema}.selitteet sty ON sty.kohde = 'maaraala_tyyppi' AND sty.tunniste = ma.tyyppi
LEFT JOIN {schema}.selitteet sms ON sms.kohde = 'maaraala_saantotapa' AND sms.tunniste = ma.saantotapa
LEFT JOIN {schema}.selitteet sml ON sml.kohde = 'maaraala_lakkaamis_syy' AND sml.tunniste = ma.lakkaamis_syy
LEFT JOIN {schema}.selitteet smt ON smt.kohde = 'maaraala_tietolahde' AND smt.tunniste = ma.tietolahde;
