CREATE OR REPLACE VIEW {schema}.rajamerkit AS
SELECT
r.fid,
r.numero,
r.sijaintitarkkuus,
r.rekisterointi_pvm,
r.laji,
srl.selite as laji_selite,
r.olemassaolo,
sro.selite as olemassaolo_selite,
r.rakenne,
srr.selite as rakenne_selite,
r.maanpinta_suhde,
srm.selite as maanpinta_suhde_selite,
r.lahdeaineisto,
sla.selite as lahdeaineisto_selite,
r.tieto_pvm,
r.geom
FROM {schema}.rajamerkki r
LEFT JOIN {schema}.selitteet srl ON srl.kohde = 'rajamerkki_laji' AND srl.tunniste = r.laji
LEFT JOIN {schema}.selitteet sro ON sro.kohde = 'rajamerkki_olemassaolo' AND sro.tunniste = r.olemassaolo
LEFT JOIN {schema}.selitteet srr ON srr.kohde = 'rajamerkki_rakenne' AND srr.tunniste = r.rakenne
LEFT JOIN {schema}.selitteet srm ON srm.kohde = 'rajamerkki_maanpinta_suhde' AND srm.tunniste = r.maanpinta_suhde
LEFT JOIN {schema}.selitteet sla ON sla.kohde = 'lahdeaineisto' AND sla.tunniste = r.lahdeaineisto;
