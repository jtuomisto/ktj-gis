CREATE OR REPLACE VIEW {schema}.omistajat AS
SELECT
row_number() over () as fid,
y.sijaintitunnus,
h.nimi,
h.osoite,
h.y_tunnus,
y.tieto_pvm
FROM {schema}.yhteystieto y
JOIN {schema}.henkilo h ON h.tunnus = y.henkilon_tunnus
ORDER BY y.sijaintitunnus;
