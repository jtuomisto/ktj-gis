CREATE OR REPLACE VIEW {schema}.kunta AS
SELECT 1 as fid, ST_BuildArea(ST_Collect(geom)) as geom
FROM {schema}.kiinteistoraja
WHERE laji = 653;
