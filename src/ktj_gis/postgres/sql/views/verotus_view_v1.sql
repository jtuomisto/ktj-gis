CREATE OR REPLACE VIEW {schema}.vero_kiinteisto_kokonaisuus AS
SELECT
k.fid,
k.sijaintitunnus,
k.kiinteisto_nimi,
k.kiinteisto_pinta_ala,
mp.pinta_ala::integer as maapohjien_pinta_ala,
rko.pinta_ala::integer as rakennusten_pinta_ala,
rko.tilavuus::integer as rakennusten_tilavuus,
mp.vero::numeric(15, 2) as maapohjien_kokonaisvero,
rak.vero::numeric(15, 2) as rakennusten_kokonaisvero,
muu.vero::numeric(15, 2) as muut_kokonaisvero,
(coalesce(mp.vero, 0) + coalesce(rak.vero, 0) + coalesce(muu.vero, 0))::numeric(15, 2)  as kokonaisvero,
k.tieto_pvm
FROM {schema}.vero_kiinteisto k
LEFT JOIN (
    SELECT
    sijaintitunnus, tieto_pvm,
    sum(kiinteistovero) as vero,
    sum(maapohja_pinta_ala) as pinta_ala
    FROM {schema}.vero_maapohja
    WHERE sijaintitunnus IS NOT NULL
    AND NOT veroton
    GROUP BY sijaintitunnus, tieto_pvm
) mp USING (sijaintitunnus, tieto_pvm)
LEFT JOIN (
    SELECT
    sijaintitunnus, tieto_pvm,
    sum(kiinteistovero) as vero
    FROM {schema}.vero_rakennus
    WHERE sijaintitunnus IS NOT NULL
    AND NOT veroton
    GROUP BY sijaintitunnus, tieto_pvm
) rak USING (sijaintitunnus, tieto_pvm)
LEFT JOIN (
    SELECT
    sijaintitunnus, tieto_pvm,
    sum(pinta_ala) as pinta_ala,
    sum(tilavuus) as tilavuus
    FROM {schema}.vero_rakennus_osa
    WHERE sijaintitunnus IS NOT NULL
    GROUP BY sijaintitunnus, tieto_pvm
) rko USING (sijaintitunnus, tieto_pvm)
LEFT JOIN (
    SELECT
    sijaintitunnus, tieto_pvm,
    sum(kiinteistovero) as vero
    FROM {schema}.vero_muu_profiili
    WHERE sijaintitunnus IS NOT NULL
    GROUP BY sijaintitunnus, tieto_pvm
) muu USING (sijaintitunnus, tieto_pvm)
WHERE k.sijaintitunnus IS NOT NULL;

CREATE OR REPLACE VIEW {schema}.vero_rakennus_kokonaisuus AS
SELECT
rak.fid,
rak.sijaintitunnus,
rak.rakennus_numero,
rak.prt,
osa.pinta_ala,
osa.tilavuus,
rak.rakennus_tyyppi,
rak.kayttotarkoitus,
rak.purkukuntoinen,
rak.kayttokelvoton,
rak.veroton,
rak.yleishyodyllinen,
rak.verotusarvo,
rak.kiinteistovero,
rak.tieto_pvm
FROM {schema}.vero_rakennus rak
LEFT JOIN (
    SELECT
    sijaintitunnus,
    rakennus_numero,
    sum(pinta_ala) as pinta_ala,
    sum(tilavuus) as tilavuus,
    tieto_pvm
    FROM {schema}.vero_rakennus_osa
    GROUP BY sijaintitunnus, rakennus_numero, tieto_pvm
) osa USING (sijaintitunnus, rakennus_numero, tieto_pvm)
WHERE rak.sijaintitunnus IS NOT NULL;
