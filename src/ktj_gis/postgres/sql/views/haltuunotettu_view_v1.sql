CREATE OR REPLACE VIEW {schema}.haltuunotettu AS
SELECT kiinteistotunnus, nimi, haltuunotto_pvm, rekisterointi_pvm, geom
FROM {schema}.haltuunotto;
