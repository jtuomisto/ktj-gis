CREATE OR REPLACE VIEW {schema}.kayttooikeusyksikkoosat_pisteet AS
SELECT
kp.fid,
kp.tunnus,
kp.numero,
kp.olotila,
kp.rekisterointi_pvm,
ky.nimi,
ky.laji,
s.selite as laji_selite,
kp.tieto_pvm,
kp.geom
FROM {schema}.kayttooikeusyksikkoosa_piste kp
LEFT JOIN {schema}.kayttooikeusyksikko ky USING (tunnus)
LEFT JOIN {schema}.selitteet s ON s.kohde = 'kayttooikeusyksikko_laji' AND s.tunniste = ky.laji;

CREATE OR REPLACE VIEW {schema}.kayttooikeusyksikkoosat_viivat AS
SELECT
kp.fid,
kp.tunnus,
kp.numero,
kp.olotila,
kp.rekisterointi_pvm,
ky.nimi,
ky.laji,
s.selite as laji_selite,
kp.tieto_pvm,
kp.geom
FROM {schema}.kayttooikeusyksikkoosa_viiva kp
LEFT JOIN {schema}.kayttooikeusyksikko ky USING (tunnus)
LEFT JOIN {schema}.selitteet s ON s.kohde = 'kayttooikeusyksikko_laji' AND s.tunniste = ky.laji;

CREATE OR REPLACE VIEW {schema}.kayttooikeusyksikkoosat_alueet AS
SELECT
kp.fid,
kp.tunnus,
kp.numero,
kp.olotila,
kp.rekisterointi_pvm,
ky.nimi,
ky.laji,
s.selite as laji_selite,
kp.tieto_pvm,
kp.geom
FROM {schema}.kayttooikeusyksikkoosa_alue kp
LEFT JOIN {schema}.kayttooikeusyksikko ky USING (tunnus)
LEFT JOIN {schema}.selitteet s ON s.kohde = 'kayttooikeusyksikko_laji' AND s.tunniste = ky.laji;
