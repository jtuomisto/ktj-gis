CREATE OR REPLACE VIEW {schema}.palstat AS
SELECT
p.fid,
p.kiinteistotunnus,
r.laji,
s.selite as laji_selite,
r.nimi,
r.rekisterointi_pvm,
(ST_Area(p.geom) / 10000)::numeric(12, 5) as pinta_ala_palsta,
r.pinta_ala_maa,
r.pinta_ala_vesi,
r.kayttotarkoitus,
r.peruskiinteisto_suhde,
r.osaluku,
r.manttaali,
r.arkistoviite,
p.tieto_pvm,
p.geom
FROM {schema}.palsta p
LEFT JOIN {schema}.rekisteriyksikko r USING (kiinteistotunnus)
LEFT JOIN {schema}.selitteet s ON s.kohde = 'rekisteriyksikko_laji' AND s.tunniste = r.laji;
