CREATE OR REPLACE VIEW {schema}.kiinteistorajat AS
SELECT
k.fid,
k.laji,
skl.selite as laji_selite,
k.luonne1,
k.luonne2,
k.rekisterointi_pvm,
k.lahdeaineisto,
sla.selite as lahdeaineisto_selite,
k.tieto_pvm,
k.geom
FROM {schema}.kiinteistoraja k
LEFT JOIN {schema}.selitteet skl ON skl.kohde = 'kiinteistoraja_laji' AND skl.tunniste = k.laji
LEFT JOIN {schema}.selitteet sla ON sla.kohde = 'lahdeaineisto' AND sla.tunniste = k.lahdeaineisto;
