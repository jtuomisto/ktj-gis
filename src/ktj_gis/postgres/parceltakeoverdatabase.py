from collections.abc import Callable
from itertools import batched
from psycopg import Cursor
from psycopg.rows import dict_row
from psycopg.sql import SQL, Identifier, Literal, Placeholder
from .pgconnection import PGConnection


class ParcelTakeoverDatabase(PGConnection):
    batch_size = 50

    def __init__(self, connection_uri: str, epsg_number: int=None, schema: str=None, pattern: str=''):
        super().__init__(connection_uri, epsg_number, schema)

        self._sch = Identifier(self._schema)
        self._pattern = Literal(pattern)

    def _get_composition_as_string(self, records: list[dict]) -> str:
        result = []

        for r in records:
            s, l = r['sijaintitunnus'], r['lakkaaminen']
            l = ' (X)' if l == 2 else ''
            result.append(f'{s}{l}')

        return '\n'.join(result)

    def _get_composition(self, parcel: str, cursor: Cursor) -> list[dict]:
        cursor.execute(SQL('''
        SELECT
        COALESCE(mtp.maara_alan_tunnus, mtp.kiinteistotunnus) as sijaintitunnus,
        COALESCE(mtp.maara_alan_lakkaaminen, mtp.rekisteriyksikon_lakkaaminen) as lakkaaminen
        FROM {schema}.muodostumistoimenpide mtp
        LEFT JOIN {schema}.muodostuminen m USING (tunnus)
        WHERE m.kiinteistotunnus = %s
        AND mtp.rekisteriyksikon_maara_aloista = FALSE
        ''').format(schema=self._sch), (parcel,))

        records = cursor.fetchall()

        if records is None or len(records) == 0:
            raise RuntimeError(f'Cannot find composition for {parcel}')

        return records

    def _parcels_for_takeover(self, cursor: Cursor) -> list[dict]:
        cursor.execute(SQL('''
        SELECT r.kiinteistotunnus, r.nimi, r.rekisterointi_pvm, p.geom, current_timestamp as tieto_pvm
        FROM (
            SELECT kiinteistotunnus, ST_Union(geom) AS geom
            FROM {schema}.palsta
            WHERE kiinteistotunnus LIKE {pattern}
            GROUP BY kiinteistotunnus
        ) p
        LEFT JOIN {schema}.rekisteriyksikko r USING (kiinteistotunnus)
        ''').format(schema=self._sch, pattern=self._pattern))

        return cursor.fetchall()

    def _get_contacts(self, composition: list[dict], cursor: Cursor) -> list[dict]:
        parcels = SQL(',').join(map(lambda x: Literal(x['sijaintitunnus']), composition))
        cursor.execute(SQL('''
        SELECT
        y.sijaintitunnus,
        string_agg(h.nimi || ' (' || COALESCE(h.y_tunnus, h.osoite) || ')', '\n') AS yhteystiedot
        FROM {schema}.henkilo h
        LEFT JOIN {schema}.yhteystieto y ON h.tunnus = y.henkilon_tunnus
        WHERE y.sijaintitunnus IN ({parcels})
        GROUP BY y.sijaintitunnus
        ''').format(schema=self._sch, parcels=parcels))

        records = cursor.fetchall()

        if records is None or len(records) == 0:
            return None

        return records

    def _insert_contacts(self, contacts: list[dict], parcel: dict, cursor: Cursor):
        if contacts is None:
            return

        query = SQL('''
        INSERT INTO {schema}.haltuunotto_yhteystiedot (
            kiinteistotunnus, muodostaja_sijaintitunnus, yhteystiedot
        ) VALUES (
            %(kiinteistotunnus)s, %(muodostaja_sijaintitunnus)s, %(yhteystiedot)s
        )
        ''').format(schema=self._sch)

        for c in contacts:
            cursor.execute(query, {
                'kiinteistotunnus': parcel['kiinteistotunnus'],
                'muodostaja_sijaintitunnus': c['sijaintitunnus'],
                'yhteystiedot': c['yhteystiedot']
            })

    def create_tables(self):
        self.execute_files([
            'haltuunotto_table_v1.sql',
            'haltuunotto_korvaus_table_v1.sql',
            'haltuunotto_yhteystiedot_table_v1.sql'
        ], 'tables')

    def create_views(self):
        self.execute_files([
            'haltuunotettu_view_v1.sql'
        ], 'views')

    def update(self, progress: Callable=None, no_contacts: bool=False):
        if progress is None:
            progress = lambda: None

        columns = ('kiinteistotunnus', 'nimi', 'muodostajat',
                   'rekisterointi_pvm', 'tieto_pvm', 'geom')
        fields = SQL(',').join(map(lambda x: Identifier(x), columns))
        values = SQL(',').join(map(lambda x: Placeholder(x), columns))
        query = SQL('''
        INSERT INTO {schema}.haltuunotto ({fields}) VALUES ({values})
        ON CONFLICT (kiinteistotunnus) DO NOTHING
        ''').format(schema=self._sch, fields=fields, values=values)

        with self.cursor() as cursor:
            cursor.row_factory = dict_row

            for batch in batched(self._parcels_for_takeover(cursor), self.batch_size):
                with self.transaction():
                    for p in batch:
                        comp = self._get_composition(p['kiinteistotunnus'], cursor)
                        p['muodostajat'] = self._get_composition_as_string(comp)
                        cursor.execute(query, p)

                        if cursor.rowcount > 0 and not no_contacts:
                            contacts = self._get_contacts(comp, cursor)
                            self._insert_contacts(contacts, p, cursor)

                        progress()
