from collections.abc import Iterable, Callable
from dataclasses import fields as dataclass_fields, asdict
from psycopg import Cursor
from psycopg.sql import SQL, Identifier, Placeholder
from psycopg.errors import DatabaseError
from ktj_gis.taxitems import *
from .pgconnection import PGConnection


class TaxDatabase(PGConnection):
    table_mapping = {
        Kiinteisto: 'vero_kiinteisto',
        Omistaja: 'vero_omistaja',
        Maapohja: 'vero_maapohja',
        Rakennus: 'vero_rakennus',
        RakennusOsa: 'vero_rakennus_osa',
        MuuProfiili: 'vero_muu_profiili'
    }

    def __init__(self, connection_uri: str, schema: str=None):
        super().__init__(connection_uri, schema=schema)
        self._queries = {}
        self._create_queries()

    def _tables_populated(self, cursor: Cursor) -> bool:
        for table in self.table_mapping.values():
            tbl = Identifier(self._schema, table)
            query = SQL('SELECT fid FROM {table} LIMIT 1').format(table=tbl)
            cursor.execute(query)

            if cursor.rowcount > 0:
                return True

        return False

    def _create_queries(self):
        for cls, table in self.table_mapping.items():
            query = SQL('INSERT INTO {table} ({fields}) VALUES ({values})')
            dfields = dataclass_fields(cls)
            self._queries[cls] = query.format(
                table=Identifier(self._schema, table),
                fields=SQL(',').join(map(lambda x: Identifier(x.name), dfields)),
                values=SQL(',').join(map(lambda x: Placeholder(x.name), dfields))
            )

    def create_tables(self):
        self.execute_files([
            'verotus_table_v1.sql',
            'verotus_selitteet_table_v1.sql'
        ], 'tables')

    def create_views(self):
        self.execute_files(['verotus_view_v1.sql'], 'views')

    def import_definitions(self):
        self.execute_files(['verotus_v1.sql'], 'definitions')

    def insertmany(self, items: Iterable[TaxItem], force: bool=False, progress: Callable=None):
        if progress is None:
            progress = lambda: None

        with self.cursor() as cursor:
            if not force and self._tables_populated(cursor):
                raise RuntimeError('Tax tables are already populated')

            with self.transaction():
                for i in items:
                    cursor.execute(self._queries[i.__class__], asdict(i))
                    progress()
