from collections.abc import Iterable, Callable
from ktj_gis.ktjitems import *
from .pgconnection import PGConnection
from .ktjqueries import *


class KTJDatabase(PGConnection):
    def __init__(self, connection_uri: str, epsg_number: int=None, schema: str=None):
        super().__init__(connection_uri, epsg_number, schema)

        self._queries = {
            Rekisteriyksikko: RekisteriyksikkoQuery(self._schema),
            RekisteriyksikkoKiinteistoraja: RekisteriyksikkoKiinteistorajaQuery(self._schema),
            RekisteriyksikkoRajamerkki: RekisteriyksikkoRajamerkkiQuery(self._schema),
            RekisteriyksikkoKaava: RekisteriyksikkoKaavaQuery(self._schema),
            Palsta: PalstaQuery(self._schema),
            Muodostuminen: MuodostuminenQuery(self._schema),
            Muodostumistoimenpide: MuodostumistoimenpideQuery(self._schema),
            MaaraAla: MaaraAlaQuery(self._schema),
            MaaraAlaRekisteriyksikko: MaaraAlaRekisteriyksikkoQuery(self._schema),
            MaaraAlaOsa: MaaraAlaOsaQuery(self._schema),
            Kayttooikeusyksikko: KayttooikeusyksikkoQuery(self._schema),
            KayttooikeusyksikkoOsa: KayttooikeusyksikkoOsaQuery(self._schema),
            Kiinteistoraja: KiinteistorajaQuery(self._schema),
            Rajamerkki: RajamerkkiQuery(self._schema),
            Henkilo: HenkiloQuery(self._schema),
            Yhteystiedot: YhteystiedotQuery(self._schema)
        }

    def _fix_item_with_types(self, item: KTJItem, only_types: set):
        if only_types is None or item._parent is None:
            return

        # If item has a parent that is not to be written into the database, act as if it was.
        if item._parent.__class__ not in only_types:
            item._parent._modified = True

    def create_tables(self):
        self.execute_files([ # Ordering is important, due to references between tables
            'rekisteriyksikko_table_v1.sql',
            'maaraala_table_v1.sql',
            'kayttooikeusyksikko_table_v1.sql',
            'muodostuminen_v1_table.sql',
            'muodostumistoimenpide_table_v1.sql',
            'palsta_table_v1.sql',
            'kiinteistoraja_table_v1.sql',
            'rajamerkki_table_v1.sql',
            'maaraalaosa_table_v1.sql',
            'kayttooikeusyksikkoosa_table_v1.sql',
            'maaraalarekisteriyksikko_table_v1.sql',
            'rekisteriyksikkokiinteistoraja_table_v1.sql',
            'rekisteriyksikkorajamerkki_table_v1.sql',
            'rekisteriyksikkokaava_table_v1.sql',
            'henkilo_table_v1.sql',
            'yhteystieto_table_v1.sql',
            'selitteet_table_v1.sql'
        ], 'tables')

    def create_views(self):
        self.execute_files([
            'kayttooikeusyksikkoosat_view_v1.sql',
            'kiinteistorajat_view_v1.sql',
            'kunta_view_v1.sql',
            'maaraalaosat_view_v1.sql',
            'omistajat_view_v1.sql',
            'palstat_view_v1.sql',
            'rajamerkit_view_v1.sql',
            'rekisteriyksikot_view_v1.sql'
        ], 'views')

    def import_definitions(self):
        self.execute_files([
            'kaavalaji_v1.sql',
            'kayttooikeusyksikko_v1.sql',
            'kiinteistoraja_v1.sql',
            'lahdeaineisto_v1.sql',
            'maaraala_v1.sql',
            'olotila_v1.sql',
            'peruskiinteisto_suhde_v1.sql',
            'rajamerkki_v1.sql',
            'rekisteriyksikko_v1.sql',
            'toimenpidelaji_v1.sql'
        ], 'definitions')

    def insertmany(self, items: Iterable[KTJItem], progress: Callable=None, only_types: set=None):
        # Add these items after all others, foreign key constraints fail otherwise
        relation_tables = {RekisteriyksikkoKiinteistoraja, RekisteriyksikkoRajamerkki}
        after = []
        # Using only_types will most likely result in database being in a weird state,
        # reading changes into it after inserting a partial result might cause issues
        flt = lambda x: only_types is None or x.__class__ in only_types
        self._counter = 0

        if progress is None:
            progress = lambda: None

        with self.cursor() as cursor:
            with self.transaction():
                for item in filter(flt, items):
                    self._fix_item_with_types(item, only_types)

                    if item.__class__ in relation_tables:
                        after.append(item)
                        continue

                    q = self._queries[item.__class__]
                    q.insert(cursor, item)
                    progress()

                for item in filter(flt, after):
                    q = self._queries[item.__class__]
                    q.insert(cursor, item)
                    progress()
