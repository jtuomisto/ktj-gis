ktj-gis
=======

Reads XML KTJ-data from [Maanmittauslaitos][MMLktj] and
tax lists from [Verohallinto][Vero] into PostgreSQL.

Dependencies
------------

Requires Python 3.12 or later.

Some dependencies might not have pre-built wheels in pypi, but Windows builds are available here:

[geospatial-wheels](https://github.com/cgohlke/geospatial-wheels)

[MMLktj]: https://www.maanmittauslaitos.fi/huoneistot-ja-kiinteistot/asiantuntevalle-kayttajalle/kiinteistotiedot-ja-niiden-hankinta
[Vero]: https://www.vero.fi/
